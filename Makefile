BASEPYTHON = `which python3`
PIP = venv/bin/pip
PYTHON = venv/bin/python

.DEFAULT_GOAL := help

clean: ## Remove virtualenv folder
	@rm -rf venv

test: ## Run unit-test on library package
	@python -m unittest tests/*.py

env: ## Create virtualenv and install dependencies
	@virtualenv venv -p $(BASEPYTHON)
	$(PIP) install -r requirements.txt

# init: ## Init the setup, deploy the DB schema
# 	$(PYTHON) manage.py init_es

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
