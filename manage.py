#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script to manage admin tasks"""

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from elasticsearch import Elasticsearch
from app.resource import db
from app.tntuser.models import User, Role
from app import app

manager = Manager(app)
es = Elasticsearch([app.config['ESHOST']])
migrate = Migrate(app, db, directory='data/migrations')

manager.add_command('db', MigrateCommand)

@manager.command
def init_es(force=False, restrict=None):
    """Function to initiate indices in ElasticSearch (ie deploy mappings)
    :param restrict: list of mappings, comma separated (default is None, ie deploy all mappings)
    """
    if not restrict or 'item' in restrict.split(','):
        index = '{}-{}'.format(app.config['ESINDEX'], "item")
        res = es.indices.exists(index=index)

        if not res or (res and force):
            if res and force:
                res = es.indices.delete(index=index)
                app.logger.info('Index deleted %s' % res)
            res = es.indices.create(index=index, body='')
            app.logger.info('Index created %s' % res)

        mapping_item = {
            "properties": {
                "map": {
                    "type": "text"
                },
                "location": {
                    "type": "geo_point"
                },
                "time": {
                    "properties": {
                        "start": {"type": "date"},
                        "end": {"type": "date"}
                    }
                },
                "data": {
                    "properties": {
                        "instagram_id": {"type": "text"},
                        "unsplash_id": {"type": "text"},
                        "title": {"type": "text"},
                        "body": {"type": "text"},
                        "tags": {"type" : "text"},
                        "author": {"type" : "integer"},
                        "location": {"type" : "text"},
                        "images": {"type" : "text"},
                        "original_link": {"type" : "text"}
                    }
                }
            }
        }
        res = es.indices.put_mapping(index=index, doc_type='doc', body=mapping_item)
        app.logger.info('mappings for item have been sent %s' % res)

    if not restrict or 'item_comment' in restrict.split(','):
        index = '{}-{}'.format(app.config['ESINDEX'], "comment")
        res = es.indices.exists(index=index)

        if not res or (res and force):
            if res and force:
                res = es.indices.delete(index=index)
                app.logger.info('Index deleted %s' % res)
            res = es.indices.create(index=index, body='')
            app.logger.info('Index created %s' % res)

        mapping_item = {
            "properties": {
                "date": {"type": "date"},
                "item": {"type": "text"},
                "text": {"type": "text"},
                "source": {"type": "text"},
                "author": {"type": "text"},
            }
        }
        res = es.indices.put_mapping(index=index, doc_type='doc', body=mapping_item)
        app.logger.info('mappings for item_comment have been sent %s' % res)

    if not restrict or 'tag' in restrict.split(','):
        index = '{}-{}'.format(app.config['ESINDEX'], "tag")
        res = es.indices.exists(index=index)

        if not res or (res and force):
            if res and force:
                res = es.indices.delete(index=index)
                app.logger.info('Index deleted %s' % res)
            res = es.indices.create(index=index, body='')
            app.logger.info('Index created %s' % res)

        mapping_tag = {
            "properties": {
                "label" : {
                    "type" : "completion"
                }
            }
        }
        res = es.indices.put_mapping(index=index, doc_type='doc', body=mapping_tag)
        app.logger.info('mappings for tag have been sent %s' % res)


@manager.command
def init_db():
    """Function to initiate Database for users"""
    print('Creating User/Role model')
    User.metadata.create_all(db.engine)
    Role.metadata.create_all(db.engine)


if __name__ == '__main__':
    manager.run()
