#!/bin/bash
#
# gunicorn script

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")
readonly LOGFILE="$PROGDIR/log/gunicorn.log"
readonly VENVBIN="$PROGDIR/venv/bin/activate"
readonly NUM_WORKERS=3
readonly TIMEOUT=120
readonly APP_CONFIG_FILE_DOCKER="$PROGDIR/config/docker.py"
readonly APP_CONFIG_FILE_PRODUCTION="$PROGDIR/config/production.py"
readonly APP_CONFIG_FILE_PREPRODUCTION="$PROGDIR/config/preproduction.py"
readonly LOGLEVEL_PRODUCTION="info"
readonly LOGLEVEL_PREPRODUCTION="debug"
readonly LOGLEVEL_DOCKER="debug"

if [[ "$#" -gt "0" ]]; then
    export APP_ENV="$1"

    if [[ "$1" == "production" ]]; then
        export CELERY_BROKER_URL="redis://127.0.0.1:6380/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6380/0"
        export APP_CONFIG_FILE="$APP_CONFIG_FILE_PRODUCTION"
        readonly LOGLEVEL="$LOGLEVEL_PRODUCTION"
        readonly ADDRESS="127.0.0.1:5001"
    elif [[ "$1" == "preproduction" ]]; then
        export CELERY_BROKER_URL="redis://127.0.0.1:6379/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6379/0"
        export APP_CONFIG_FILE="$APP_CONFIG_FILE_PREPRODUCTION"
        readonly LOGLEVEL="$LOGLEVEL_PREPRODUCTION"
        readonly ADDRESS="127.0.0.1:5000"
    elif [[ "$1" == "docker" ]]; then
        export CELERY_BROKER_URL="redis://127.0.0.1:6379/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6379/0"
        export APP_CONFIG_FILE="$APP_CONFIG_FILE_DOCKER"
        readonly LOGLEVEL="$LOGLEVEL_DOCKER"
        readonly ADDRESS="127.0.0.1:5000"
    fi
fi

# shellcheck source=/dev/null
source "$VENVBIN"

exec gunicorn app:app \
  --workers $NUM_WORKERS \
  --timeout $TIMEOUT \
  --bind=$ADDRESS \
  --log-level=$LOGLEVEL \
  --log-file="$LOGFILE" 2>> "$LOGFILE"
