======================================
There 'n Then - Installation procedure
======================================

Dependencies
=============

You'll need to have the following components alive and kicking:

* nginx
* ElasticSearch
* python 3.5


Installation
=============

Create the user and environment:

.. code-block:: bash

    # useradd -g the-flying-trout -m -s /bin/bash tnt
    # su - tnt
    tnt@vps115485:~$ ssh-keygen -b 2048
    # mkdir /var/www/tnt.the-flying-trout.com/
    # chown -R tnt:the-flying-trout /var/www/tnt.the-flying-trout.com/


Deploy the code:

.. code-block:: bash

    $ ssh tnt@the-flying-trout.com
    tnt@vps115485:~$ cd /var/www/

    tnt@vps115485:/var/www$ git clone git@gitlab.com:the-flying-trout/there-n-then.git tnt.the-flying-trout.com/
    Cloning into 'tnt.the-flying-trout.com'...
    The authenticity of host 'gitlab.com (52.167.219.168)' can't be established.
    ECDSA key fingerprint is f1:d0:fb:46:73:7a:70:92:5a:ab:5d:ef:43:e2:1c:35.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added 'gitlab.com,52.167.219.168' (ECDSA) to the list of known hosts.
    remote: Counting objects: 220, done.
    remote: Compressing objects: 100% (158/158), done.
    remote: Total 220 (delta 56), reused 185 (delta 44)
    Receiving objects: 100% (220/220), 1.57 MiB | 2.32 MiB/s, done.
    Resolving deltas: 100% (56/56), done.
    Checking connectivity... done.

    tnt@vps115485:/var/www$ cd tnt.the-flying-trout.com/
    tnt@vps115485:/var/www/tnt.the-flying-trout.com$ git checkout develop
    Branch develop set up to track remote branch develop from origin.
    Switched to a new branch 'develop'

Let's test the application works:

.. code-block:: bash

    tnt@vps115485:/var/www/tnt.the-flying-trout.com$ make env
    tnt@vps115485:/var/www/tnt.the-flying-trout.com$ source venv/bin/activate
    (venv)tnt@vps115485:/var/www/tnt.the-flying-trout.com$ ./run.py
     * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
     * Restarting with stat
     * Debugger is active!
     * Debugger PIN: 182-143-107

Sounds good !

Now let's configure nginx:

.. code-block:: bash

    # vim /etc/nginx/conf.d/tnt.the-flying-trout.com.conf

.. code-block::

    upstream tnt_server {
        server localhost:5000 fail_timeout=0;
    }

    server {
        listen       80;
        server_name  tnt.the-flying-trout.com;
        # enforce https
        return 301 https://tnt.the-flying-trout.com$request_uri;
    }

    server {
        listen 443 ssl;
        server_name tnt.the-flying-trout.com;

        access_log  /var/log/nginx/tnt.the-flying-trout.com.access.log main;
        error_log  /var/log/nginx/tnt.the-flying-trout.com.error.log info;

        location /.well-known {
            root /var/www/tnt.the-flying-trout.com/app/static/;
        }

        location /static {
            root /var/www/tnt.the-flying-trout.com/app/;
        }

        location / {
            proxy_pass http://tnt_server;
            proxy_http_version 1.1;
            proxy_set_header X-Forwarded-For $remote_addr;
            if ($uri != '/') {
                expires 30d;
            }
        }

    }

.. code-block:: bash

    # service nginx reload


Run Dependencies in Docker
==========================

In order to run the services in Docker container, you can fire the following commands:

.. code-block:: bash
    
    # docker run -it -v $(pwd)/data/elasticsearch:/usr/share/elasticsearch/data/ -p "9200:9200" elasticsearch:5-alpine
    # docker run -it -v $(pwd)/data/redis:/var/lib/redis/data/ -p "6379:6379" redis:4.0-alpine



MISC
====

Renew certificate:

.. code-block:: bash

    # certbot certonly --webroot -w /var/www/the-flying-trout.com -d the-flying-trout.com -d www.the-flying-trout.com -w /var/www/tnt.the-flying-trout.com/app/static -d tnt.the-flying-trout.com  -w /var/www/there-n-then.co/app/static -d there-n-then.co -d www.there-n-then.co 
