#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Main configuration"""

import os
import logging

## General configuration
DEBUG = True
TRAP_HTTP_EXCEPTIONS = False
SECRET_KEY = b'\xa7\x1cv7\x8c\x8aHQ\x03\xc1\xb0M\x13\x00\xac%\x1c\xb2\xe6\x8b\x9f+)\xba'
SECURITY_PASSWORD_SALT = 'Fi76C8MTh27r54Wr92tuV3ji6HkxRhNJ'
BCRYPT_LOG_ROUNDS = 13
ROOT_URL = 'http://localhost:5000'
GOOGLEMAPS_API_KEY = 'AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58'
GOOGLETAGMANAGER_API_KEY = 'GTM-5QTMRNL'

## Paths
APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top

## ElasticSearch
ESHOST = '127.0.0.1'
ESINDEX = 'tnt'
ES_DEFAULT_SIZE = 50

# Logs
LOG_ENABLE_FILE = True
LOG_LEVEL = logging.DEBUG
LOG_FILE = 'log/therenthen.log'
LOG_MAX_BYTES = 10000000
LOG_BACKUP_COUNT = 42

# Mail
MAIL_DEFAULT_CONTACT = 'hello@therenthen.co'
MAIL_SUPPORT_CONTACT = 'support@therenthen.co'
MAIL_DEFAULT_SENDER = 'hello@therenthen.co'
MAIL_SERVER = 'in-v3.mailjet.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USERNAME = '5705277be9e55d9cc8d1a0289078a269'
MAIL_PASSWORD = 'ce4869ea5988668da4e35c9918b43564'

# User storage
DB_NAME = 'data/tntuser.sqlite'
DB_PATH = os.path.join(APP_ROOT, '..', DB_NAME)
SQLALCHEMY_DATABASE_URI = 'sqlite:///{0}'.format(DB_PATH)
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Instagram connection
INSTAGRAM_KEY = '72f501f4fbf34245a0e3018083adb0a6'
INSTAGRAM_SECRET = '8cf6aa0afaf54091a4ca3be64bede917'
INSTAGRAM_SCOPE = ['basic']

# Unsplas application
UNSPLASH_KEY = 'fa2c8224858f34df3aee3f440661a51f75834bef1ca6bc3b4050f8586b936621'
UNSPLASH_SECRET = '0c25ea2f2f632a64871eb3b44a3af739fca6f1c050fd2f32cf357e9f94ea16f0'
UNSPLASH_SCOPE = ['public']

# SOCIAL configuration
SOCIAL_AUTH_STORAGE = 'social_flask_sqlalchemy.models.FlaskStorage'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_USER_MODEL = 'app.tntuser.models.User'
SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ['keep']
SOCIAL_AUTH_FORCE_EMAIL_VALIDATION = True  # WIP: should be set to True (http://psa.matiasaguirre.net/docs/pipeline.html#email-validation)
SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
    'social.backends.email.EmailAuth',
    'social.backends.instagram.InstagramOAuth2',
)
SOCIAL_AUTH_PIPELINE = (
    # Get the information we can about the user and return it in a simple
    # format to create the user instance later. On some cases the details are
    # already part of the auth response from the provider, but sometimes this
    # could hit a provider API.
    'social_core.pipeline.social_auth.social_details',

    # Get the social uid from whichever service we're authing thru. The uid is
    # the unique identifier of the given user in the provider.
    'social_core.pipeline.social_auth.social_uid',

    # Verifies that the current auth process is valid within the current
    # project, this is where emails and domains whitelists are applied (if
    # defined).
    'social_core.pipeline.social_auth.auth_allowed',

    # Checks if the current social-account is already associated in the site.
    'social_core.pipeline.social_auth.social_user',

    # Make up a username for this person, appends a random string at the end if
    # there's any collision.
    'social_core.pipeline.user.get_username',

    # Send a validation email to the user to verify its email address.
    # Disabled by default.
    'social_core.pipeline.mail.mail_validation',

    # Associates the current social details with another user account with
    # a similar email address. Disabled by default.
    'social_core.pipeline.social_auth.associate_by_email',

    # Create a user account if we haven't found one yet.
    'social_core.pipeline.user.create_user',

    # Create the record that associates the social account with the user.
    'social_core.pipeline.social_auth.associate_user',

    # Populate the extra_data field in the social record with the values
    # specified by settings (and the default ones like access_token, etc).
    'social_core.pipeline.social_auth.load_extra_data',

    # Update the user record with any changed info from the auth service.
    'social_core.pipeline.user.user_details',
)
SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'app.tntuser.utils.send_validation_email'
