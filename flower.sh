#!/bin/bash
#
# celery script

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")
readonly LOGFILE="$PROGDIR/log/flower.log"
readonly VENVBIN="$PROGDIR/venv/bin/activate"
readonly LOGLEVEL_PRODUCTION="info"
readonly LOGLEVEL_PREPRODUCTION="debug"

if [[ "$#" -gt "0" ]]; then
    if [[ "$1" == "production" ]]; then
        readonly LISTENADDRESS="127.0.0.1"
        readonly LISTENPORT=5556
        readonly LOGLEVEL="$LOGLEVEL_PRODUCTION"
        export CELERY_BROKER_URL="redis://127.0.0.1:6380/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6380/0"
    elif [[ "$1" == "preproduction" ]]; then
        readonly LISTENADDRESS="127.0.0.1"
        readonly LISTENPORT=5555
        readonly LOGLEVEL="$LOGLEVEL_PREPRODUCTION"
        export CELERY_BROKER_URL="redis://127.0.0.1:6379/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6379/0"
    fi
fi

# shellcheck source=/dev/null
source "$VENVBIN"

exec celery flower \
  -A app.resource:celery \
  --url_prefix=flower \
  --address=$LISTENADDRESS --port=$LISTENPORT \
  -l $LOGLEVEL >> "$LOGFILE" 2>&1
