#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module is the main Flask application, the entry point for all There 'n Then
"""

# all the imports
import os
import datetime
from time import time
from logging import Formatter
from logging.handlers import RotatingFileHandler
from flask import Flask, g, render_template
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from werkzeug.exceptions import HTTPException
from flask_wtf.csrf import CSRFProtect
from flask_gravatar import Gravatar
import flask_login
from raven.contrib.flask import Sentry

## create our little application :)
app = Flask(
    __name__,
    instance_path=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'instance'),
    instance_relative_config=True)

## Init Sentry
sentry = Sentry(app, dsn='https://369ae5f8d0c9412286b957219db63aeb:42eaf34048a6495f9b06b3fdeef15b94@sentry.io/1218462')

from app.resource import connect_es, init_mail, db, login_manager, bcrypt
from app.tntpage.views import tntpage
from app.tntopen.views import tntopen
from app.tntmap.views import tntmap
from app.tntitem.views import tntitem
from app.tnttag.views import tnttag
from app.tntuser.views import tntuser
from app.tntuser.models import User, Role
from app.tntuser.utils import send_email

## Configuration
# Load the configuration
app.config.from_object(__name__)
app.config.from_object('config.default')

# Handle error pages
@app.errorhandler(400)
@app.errorhandler(401)
@app.errorhandler(404)
@app.errorhandler(405)
@app.errorhandler(500)
def handle_error(e):
    """Handle HTTP errors"""
    try:
        code = e.code
    except AttributeError as e:
        code = 500

    if code != 404:
        sentry.captureMessage('We had en error: {}'.format(e))

    return render_template('error.html', error=e), code


## Init dependencies
@app.before_request
def before_request():
    """Before handling requests"""
    g.es = connect_es()
    g.user = flask_login.current_user
    g.mail = init_mail()


# Make current user available on templates
@app.context_processor
def inject_user():
    """Before rendering result"""
    try:
        return {'user': g.user}
    except AttributeError:
        return {'user': None}

# Make Google Maps API and GTM keys available on templates
@app.context_processor
def inject_google_keys():
    """Before rendering result"""
    return {
        'googlemaps_key': app.config.get('GOOGLEMAPS_API_KEY'),
        'gtm_key': app.config.get('GOOGLETAGMANAGER_API_KEY')}

# Make Google Maps API and GTM keys available on templates
@app.context_processor
def inject_base_url():
    """Before rendering result"""
    return {
        'base_url': app.config.get('ROOT_URL')}

@app.teardown_appcontext
def commit_on_success(error=None):
    """Before closing the application"""
    if error is None:
        db.session.commit()

# Configure Gravatar
gravatar = Gravatar(
    app,
    size=150,
    rating='g',
    default='retro',
    force_default=False,
    force_lower=False,
    use_ssl=True,
    base_url=None)

@app.template_filter('ctime')
def timectime(s):
    """convert a timetamp to a datetime"""
    if s is not None and isinstance(s, (int, str)):
        return datetime.datetime.fromtimestamp(
            int(s)
        ).strftime('%Y-%m-%d %H:%M')
    else:
        return ""

@app.template_filter('cdate')
def timecdate(s):
    """convert a timetamp to a date"""
    if s is not None and isinstance(s, (int, str)):
        return datetime.datetime.fromtimestamp(
            int(s)
        ).strftime('%Y-%m-%d')
    else:
        return ""

# Load the file specified by the APP_CONFIG_FILE environment variable
# (absolute path to platform specific config file)
if os.environ.get('APP_CONFIG_FILE', None):
    print('loading %s config' % os.environ.get('APP_CONFIG_FILE'))
    app.config.from_envvar('APP_CONFIG_FILE')

# Logs
appLogFile = app.config['LOG_FILE']
if appLogFile is not None:
    file_handler = RotatingFileHandler(
        appLogFile,
        maxBytes=app.config['LOG_MAX_BYTES'],
        backupCount=app.config['LOG_BACKUP_COUNT'])
    file_handler.setLevel(app.config['LOG_LEVEL'])
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))
    app.logger.addHandler(file_handler)

# Register extentions
bcrypt.init_app(app)
login_manager.init_app(app)
csrf = CSRFProtect(app)
db.init_app(app)

# Admin interface
class TnTUserModelView(ModelView):
    page_size = 50  # the number of entries to display on the list view
    column_exclude_list = ['password', 'insta_token']
    column_searchable_list = ['username', 'email']
    column_editable_list = ['active']
    form_excluded_columns = ['password']
    create_modal = True
    edit_modal = True


admin = Admin(app, name='ThereNThen', template_mode='bootstrap3')
admin.add_view(TnTUserModelView(User, db.session))
admin.add_view(ModelView(Role, db.session))

# register blueprints
app.register_blueprint(tntpage)
app.register_blueprint(tntopen)
app.register_blueprint(tntmap)
app.register_blueprint(tntitem)
app.register_blueprint(tnttag)
app.register_blueprint(tntuser)
