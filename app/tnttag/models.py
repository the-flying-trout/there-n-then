#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Item models definition"""

from flask import g
from elasticsearch import ElasticsearchException
from app import app


class TntTag(object):
    """Point in space and time"""
    def __init__(self, _id=None, label=None):
        """Define a TntTag object
        :param label: label of the tag
        :return: the id of the item
        """
        super(TntTag, self).__init__()

        self._id = _id
        self.label = label


    def __str__(self):
        """Return a string representation of TntTag"""
        return '{}'.format(self.label)


    def save(self):
        """Save a TntTag in the ES"""
        doc = {
            'label': {
                'input': self.label
            },
        }

        if self._id:
            try:
                res = g.es.index(index='{}-{}'.format(app.config['ESINDEX'], 'tag'), doc_type='doc', id=self._id, body=doc)
            except ElasticsearchException as e:
                app.logger.error('ES Error while saving tag %s: %s', (doc, e))
                self._id = None
        else:
            try:
                res = g.es.index(index='{}-{}'.format(app.config['ESINDEX'], 'tag'), doc_type='doc', body=doc)
                self._id = res['_id']
            except ElasticsearchException as e:
                app.logger.error('ES Error while searching tag %s: %s', (doc, e))
                self._id = None

        return self._id


    def delete(self):
        """Delete a TntTag in the ES"""
        retour = False

        if self._id:
            try:
                res = g.es.delete(index='{}-{}'.format(app.config['ESINDEX'], 'tag'), doc_type='doc', id=self._id)
                retour = res['found']
            except ElasticsearchException as e:
                app.logger.error('ES Error while deleting items %s: %s', (self._id, e))
                retour = False

        return retour


class TntTags(object):
    """Collection of TntItem"""
    def __init__(self, items=None):
        super(TntTags, self).__init__()
        self.items = []

        if items is not None:
            for item in items:
                self.items.append(item)


    def add(self, item=None):
        """Add a TntItem to the collection"""
        if item is not None:
            self.items.append(item)


    def search(self, text=None):
        """Build a collection from a search to ES
        :param text: free text to search in title or body
        """
        retour = 0

        if text is not None and text != "":
            query = {
                "_source": "label",
                "suggest": {
                    "tag-suggest": {
                        "prefix": text,
                        "completion" : {
                            "field": "label"
                        }
                    }
                }
            }

            # app.logger.debug(query)

            try:
                cur = g.es.search(
                    index='{}-{}'.format(app.config['ESINDEX'], 'tag'),
                    doc_type='doc',
                    body=query,
                    size=20)
            except ElasticsearchException as e:
                app.logger.error('ES Error while searching tags %s: %s', (query, e))
                cur = None

            if cur is not None and len(cur['suggest']['tag-suggest'][0].get('options')) > 0:
                for row in cur['suggest']['tag-suggest'][0].get('options'):
                    self.add(TntTag(
                        _id=row['_id'],
                        label=row['_source']['label']['input'],
                    ))

                retour = len(cur['suggest']['tag-suggest'][0].get('options'))

        return retour
