#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utility functions for Tag management"""

def enrich_items(items=None):
    """Add extra data to list of items
    :param items: list of TntTag
    :return: list of tnttag
    """
    uniq_items = set()
    enriched_items = list()

    if items is not None:
        for item in items:
            uniq_items.add(str(item))

    for item in uniq_items:
        enriched_items.append({'tag': item})

    return enriched_items
