#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This modules handles all the tag actions
"""

# all the imports
from flask import Blueprint, jsonify, request
from app.tnttag.models import TntTags, TntTag
from app.tnttag.utils import enrich_items


# Define the blueprint: 'maps', set its url prefix: app.url
tnttag = Blueprint(
    'tnttag',
    __name__,
    url_prefix='/tag',
    template_folder='templates',
    static_folder='../static')


@tnttag.route('/search')
def search_tags():
    """Function to search a list of tags"""
    my_collection = TntTags()
    my_collection.search(text=request.args.get('tag'))

    items = enrich_items(my_collection.items)

    return jsonify(items)

@tnttag.route('/add', methods=['GET'])
def add_tag():
    """Function to add a tag"""
    my_tag = TntTag(label=request.args.get('tag'))
    my_tag.save()

    return jsonify({'tag': str(my_tag)})
