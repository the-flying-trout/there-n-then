#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Forms for the items."""

from flask_wtf import FlaskForm
from wtforms import HiddenField, TextField, TextAreaField
from wtforms.validators import DataRequired, Optional, Regexp
from app.tntitem.utils import date_to_ts
from app import app

class SearchForm(FlaskForm):
    """Search form."""

    keyword = TextField(
        'Keyword'
    )
    tags = TextField(
        'Tags',
        id='input-tags',
    )
    place = TextField(
        'Place',
        id='address-autocomplete',
    )
    event_start = HiddenField(
        'event_start',
        id='event_start',
        validators=[Optional(), Regexp(r'\d{4}[-/]\d{2}[-/]\d{2}')]
    )
    event_end = HiddenField(
        'event_end',
        id='event_end',
        validators=[Optional(), Regexp(r'\d{4}[-/]\d{2}[-/]\d{2}')]
    )

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(SearchForm, self).__init__(*args, **kwargs)

    def validate(self):
        """Validate the form."""
        initial_validation = super(SearchForm, self).validate()

        if not initial_validation:
            app.logger.debug(self.errors)
            return False

        if (self.keyword.data == "" and
                self.place.data == "" and
                self.tags.data == "" and
                self.event_start.data == ""):
            return False

        if self.event_start.data is not None:
            self.event_start.data = date_to_ts(str(self.event_start.data))
        if self.event_end.data is not None:
            self.event_end.data = date_to_ts(str(self.event_end.data))

        return True


class EditForm(FlaskForm):
    """Search form."""

    iditem = HiddenField('iditem')
    title = TextField(
        'Title',
        validators=[DataRequired()]
    )
    body = TextAreaField(
        'Description',
        validators=[Optional()]
    )
    tags = TextField(
        'Tags',
        validators=[Optional()]
    )
    event_start = TextField(
        'event_start',
        validators=[DataRequired()]
    )
    event_end = TextField(
        'event_end',
        validators=[Optional()]
    )
    latitude = HiddenField(
        'lat',
        validators=[DataRequired()]
    )
    longitude = HiddenField(
        'lon',
        validators=[DataRequired()]
    )
    images = HiddenField(
        'images',
        validators=[DataRequired()]
    )
    location = TextField(
        'location',
        id='address-autocomplete',
        validators=[DataRequired()]
    )
    instagram_id = HiddenField(
        'instagram_id',
        validators=[Optional()]
    )
    unsplash_id = HiddenField(
        'unsplash_id',
        validators=[Optional()]
    )
    original_url = HiddenField(
        'original_url',
        validators=[Optional()]
    )

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(EditForm, self).__init__(*args, **kwargs)

    def validate(self):
        """Validate the form."""
        initial_validation = super(EditForm, self).validate()

        if not initial_validation:
            app.logger.debug(self.errors)
            return False

        if self.event_end.data is None:
            self.event_end.data = self.event_start.data

        return True


class DeleteForm(FlaskForm):
    """Delete form."""

    iditem = HiddenField('iditem')

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(DeleteForm, self).__init__(*args, **kwargs)

    def validate(self):
        """Validate the form."""
        initial_validation = super(DeleteForm, self).validate()

        if not initial_validation:
            app.logger.debug(self.errors)
            return False

        return True
