#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utility functions for Item management"""

import json
import calendar
import re
from datetime import datetime
import dateutil.parser
import requests
import googlemaps
from flask import g
from app.tntitem.models import TntItem, TntItems, TntItemComment, TntItemComments
from app.tnttag.models import TntTag
from app.tntuser.models import User
from app.resource import celery, connect_es, db
from app import app

def get_insta_items(instagram_user=None, access_token=None, max_id=None, min_id=None, count=50):
    """Get the Instagram items
    :param instagram_user: A valid Instagram user
    :param access_token: A valid access token.
    :param max_id: get media earlier than this max_id
    :param min_id: get media later than this min_id
    :param count: count of media to return
    :return: list of instagram media
    """
    retour = None
    insta_url = 'https://api.instagram.com/v1/users/{0}' \
            '/media/recent/?access_token={1}&count={2}'.format(
                instagram_user,
                access_token,
                count)

    if max_id is not None:
        insta_url = insta_url + '&max_id={0}'.format(max_id)
    if min_id is not None:
        insta_url = insta_url + '&min_id={0}'.format(min_id)

    if access_token is not None and instagram_user is not None:
        try:
            r = requests.get(insta_url)
            app.logger.debug('Instagram call - %s', insta_url)
        except requests.exceptions.ConnectionError as e:
            app.logger.error('unable to connect to Instagram - %s', e)
        except requests.exceptions.RequestException as e:
            app.logger.error('unable to connect to Instagram - %s', e)

        if r.status_code != 200:
            app.logger.error('Could not get Instagram media: %s', r.reason)
            if r.status_code == 429:
                app.logger.error('Over API ratelimit')
        else:
            retour = json.loads(r.text)

    if retour:
        return retour.get('data')
    else:
        return None


def get_insta_comments(item_id=None, access_token=None):
    """Get the Instagram comments for an item
    :param item_id: Media for which we want comments
    :param access_token: A valid access token.
    :return: list of comments
    """
    retour = None
    insta_url = 'https://api.instagram.com/v1/media/{0}' \
            '/comments?access_token={1}'.format(
                item_id,
                access_token)

    if access_token is not None and item_id is not None:
        try:
            r = requests.get(insta_url)
            app.logger.debug('Instagram call - %s', insta_url)
        except requests.exceptions.ConnectionError as e:
            app.logger.error('unable to connect to Instagram - %s', e)
        except requests.exceptions.RequestException as e:
            app.logger.error('unable to connect to Instagram - %s', e)

        if r.status_code != 200:
            app.logger.error('Could not get Instagram comments: %s', r.reason)
        else:
            retour = json.loads(r.text)

    if retour:
        return retour.get('data')
    else:
        return None


def get_unsplash_items(unsplash_user=None, access_token=None, page=None, count=50):
    """Get the Unsplash items
    :param unsplash_user: A valid Unsplash user
    :param access_token: A valid access token.
    :param page: pagination page number
    :param count: count of media to return
    :return: list of unsplash media
    """
    retour = None
    headers = {
        "Authorization":"Bearer {}".format(access_token),
        "Accept-Version": "v1"
    }    
    unsplash_url = 'https://api.unsplash.com/users/{0}/photos' \
            '?page={1}&per_page={2}'.format(
                unsplash_user,
                page,
                count)

    if access_token is not None and unsplash_user is not None:
        try:
            r = requests.get(unsplash_url, headers=headers)
            app.logger.debug('Unsplash call - %s', unsplash_url)
        except requests.exceptions.ConnectionError as e:
            app.logger.error('unable to connect to Unsplash - %s', e)
        except requests.exceptions.RequestException as e:
            app.logger.error('unable to connect to Unsplash - %s', e)

        if r.status_code != 200:
            app.logger.debug('Could not get Unsplash media: %s', r.reason)
            if r.headers.get('X-Ratelimit-Remaining') and r.headers['X-Ratelimit-Remaining']<10:
                app.logger.error('Over API ratelimit')
        else:
            retour = json.loads(r.text)

    return retour


def get_unsplash_item_details(access_token=None, unsplash_id=None):
    """Get the Unsplash item full details
    :param unsplash_id: The Unsplash photo ID
    :param access_token: A valid access token.
    :return: full unsplash media (see https://unsplash.com/documentation#get-a-photo)
    """
    retour = None
    headers = {
        "Authorization":"Bearer {}".format(access_token),
        "Accept-Version": "v1"
    }    
    unsplash_url = 'https://api.unsplash.com/photos/{0}'.format(unsplash_id)

    if access_token is not None and unsplash_id is not None:
        try:
            r = requests.get(unsplash_url, headers=headers)
            app.logger.debug('Unsplash call - %s', unsplash_url)
        except requests.exceptions.ConnectionError as e:
            app.logger.error('unable to connect to Unsplash - %s', e)
        except requests.exceptions.RequestException as e:
            app.logger.error('unable to connect to Unsplash - %s', e)

        if r.status_code != 200:
            app.logger.error('Could not get Unsplash media: %s', r.reason)
        else:
            retour = json.loads(r.text)

    return retour


@celery.task(bind=True)
def sync_insta_items(self, user_id):
    """Get the items from Instagram account
    :param user_id: User object id attribute
    :return: count of item crawled
    """
    count_items = 0
    with app.app_context():
        g.es = connect_es()
        u = User.get_by_id(user_id)
        # Check existing sync being processed
        if u.insta_current_sync is not None:
            app.logger.debug('User %s trying to spawn concurrent sync processes', u.id)
            return {'total': count_items, 'status': 'done'}
        else:
            u.insta_current_sync = self.request.id
            db.session.add(u)
            db.session.commit()

        min_id = u.insta_last_id
        max_id = None
        last_id = None

        items = get_insta_items(
            instagram_user=u.insta_id, access_token=u.insta_token, max_id=max_id, min_id=min_id)
        while items is not None and len(items) > 0:
            for item in items:
                if last_id is None:
                    last_id = item.get('id')
                max_id = item.get('id')

                if (item.get('type') == 'image' and
                        item.get('location') is not None and
                        TntItems().search_by_instagram(instagram_id=item.get('id')) == 0):
                    count_items += 1

                    tntitem = insta_to_tnt_item(item=item, user_id=user_id)
                    if tntitem is not None:
                        monid = tntitem.save()
                    else:
                        app.logger.error('Unable to create the item %s' % tntitem)

                    item_comments = get_insta_comments(
                        item_id=item.get('id'), access_token=u.insta_token)
                    if item_comments:
                        for comment in item_comments:
                            item_comment = TntItemComment(
                                item=monid,
                                text=comment.get('text'),
                                source='instagram',
                                author=comment['from'].get('username'),
                                date=comment.get('created_time'))

                            item_comment.save()

                    if item.get('tags'):
                        for tag in item.get('tags'):
                            tnttag = TntTag(label=tag)
                            tnttag.save()

                    # Update the clerey status
                    self.update_state(state='PROGRESS', meta={
                        'total': count_items,
                        'status': 'in progress'})

            items = get_insta_items(
                instagram_user=u.insta_id, access_token=u.insta_token, max_id=max_id, min_id=min_id)

        u.insta_last_id = last_id
        u.insta_current_sync = None
        u.items_count += count_items
        db.session.add(u)
        db.session.commit()

    return {'total': count_items, 'status': 'done'}


def insta_to_tnt_item(item=None, user_id=None):
    """Transform an Instagram item into a TnT item
    :param item: Instagram item (see https://www.instagram.com/developer/endpoints/media/)
    :param user_id: User object id attribute
    :return: TnT item object
    """
    retour = None

    if item is not None:
        titre_object = item['caption']
        if titre_object is not None:
            clean_titre = clean_insta_title(title=titre_object.get('text'))
            clean_body = titre_object.get('text')
        else:
            clean_titre = ''
            clean_body = ''
        original_url = '<a href="{0}">Original Instagram photo</a>'.format(item.get('link'))

        if (item.get('images') is not None and
                item['images'].get('standard_resolution') is not None and
                item['images']['standard_resolution'].get('url') is not None):
            images_list = [item['images']['standard_resolution'].get('url')]
        else:
            return retour

        if item.get('carousel_media'):
            for element in item['carousel_media']:
                if (element.get('type') == 'image' and
                        element.get('images') is not None and
                        element['images'].get('standard_resolution') is not None and
                        element['images']['standard_resolution'].get('url') is not None):
                    images_list.append(element['images']['standard_resolution']['url'])

        if item.get('location') is not None:
            location_name = item['location'].get('name')
            location_d = {'lat': item['location'].get('latitude'), 'lon': item['location'].get('longitude')}
        else:
            location_name = ''
            location_d = {'lat': None, 'lon': None}

        time_t = (item['created_time'], item['created_time'])

        data = {
            'instagram_id': item.get('id'),
            'title': clean_titre,
            'body': clean_body,
            'author': user_id,
            'tags': item.get('tags'),
            'location': location_name,
            'images': images_list
        }

        retour = TntItem(location_d=location_d, time_t=time_t, data_d=data)

    return retour


@celery.task(bind=True)
def sync_unsplash_items(self, user_id):
    """Get the items from Unsplash account
    :param user_id: User object id attribute
    :return: count of item crawled
    """
    count_items = 0
    with app.app_context():
        g.es = connect_es()
        u = User.get_by_id(user_id)
        # Check existing sync being processed
        if u.unsplash_current_sync is not None:
            app.logger.debug('User %s trying to spawn concurrent sync processes', u.id)
            return {'total': count_items, 'status': 'done'}
        else:
            u.unsplash_current_sync = self.request.id
            db.session.add(u)
            db.session.commit()

        page = 1
        last_id = None

        items = get_unsplash_items(
            unsplash_user=u.unsplash_user, access_token=u.unsplash_token, page=page)
        while items is not None and len(items) > 0:
            for item in items:
                if last_id is None:
                    last_id = item.get('id')

                if (TntItems().search_by_unsplash(unsplash_id=item.get('id')) == 0):
                    count_items += 1

                    full_item = get_unsplash_item_details(
                        access_token=u.unsplash_token, unsplash_id=item.get('id'))

                    tntitem = unsplash_to_tnt_item(item=full_item, user_id=user_id)
                    if tntitem is not None:
                        monid = tntitem.save()
                    else:
                        app.logger.error('Unable to create the item: %s' % tntitem)

                    # Update the celery status
                    self.update_state(state='PROGRESS', meta={
                        'total': count_items,
                        'status': 'in progress'})
                else:
                    break

            page += 1
            items = get_unsplash_items(
                unsplash_user=u.unsplash_user, access_token=u.unsplash_token, page=page)

        u.unsplash_last_id = last_id
        u.unsplash_current_sync = None
        u.items_count += count_items
        db.session.add(u)
        db.session.commit()

    return {'total': count_items, 'status': 'done'}


def unsplash_to_tnt_item(item=None, user_id=None):
    """Transform an Unsplash item into a TnT item
    :param item: Unsplash item (see https://unsplash.com/documentation#get-a-photo)
    :param user_id: User object id attribute
    :return: TnT item object
    """
    retour = None

    if item is not None:
        titre_object = item['description']
        if titre_object is not None:
            clean_titre = clean_insta_title(title=titre_object)
            clean_body = clean_titre
        else:
            clean_titre = ''
            clean_body = ''
        unsplash_username = item['user'].get('name')
        original_url = 'Photo by <a href="https://unsplash.com/@{0}?utm_source=There%20n%20Then&utm_medium=referral&utm_campaign=api-credit" target="_blank">{1}</a> / <a href="https://unsplash.com/?utm_source=There%20n%20Then&utm_medium=referral&utm_campaign=api-credit" target="_blank">Unsplash</a>'.format(item['user'].get('username'), unsplash_username)

        images_list = [item['urls'].get('small')]

        if (item.get('location') is not None and
            item['location'].get('position') is not None):
            if item['location'].get('title') is not None:
                location_name = item['location']['title']
            else:
                location_name = get_geoplace(
                    lat=item['location']['position'].get('latitude'),
                    lon=item['location']['position'].get('longitude'))
            location_d = {
                'lat': item['location']['position'].get('latitude'),
                'lon': item['location']['position'].get('longitude')}
        else:
            location_name = ''
            location_d = {'lat': None, 'lon': None}

        item_date = dateutil.parser.parse(item['created_at'])
        clean_date = item_date.strftime('%s')
        time_t = (clean_date, clean_date)

        tags_list = []
        if item.get('categories') and len(item['categories'])>0:
            for category in item['categories']:
                tags_list.append(clean_unsplash_tag(category.get('title')))

        data = {
            'unsplash_id': item.get('id'),
            'title': clean_titre,
            'body': clean_body,
            'author': user_id,
            'tags': tags_list,
            'location': location_name,
            'images': images_list,
            'original_url': original_url
        }

        retour = TntItem(location_d=location_d, time_t=time_t, data_d=data)

    return retour


def enrich_items(items=None):
    """Add extra data to list of items
    :param items: list of ES items
    :return: list of tntitem
    """
    enriched_items = []

    if items is not None:
        for item in items:
            (latitude, longitude) = (item.location.get('lat'), item.location.get('lon'))
            u = User.get_by_id(item.data['author'])
            comments = TntItemComments()
            comments.search(item=item._id)

            clean_comments = []
            for comment in comments.items:
                if comment.source == 'tnt':
                    author = User.get_by_id(comment.author)
                    author_name = author.full_name
                else:
                    author_name = comment.author
                clean_comments.append({
                    'date': comment.date,
                    'text': comment.text,
                    'source': comment.source,
                    'author': author_name})

            if item.data.get('original_url'):
                original_url = item.data['original_url']
            else:
                original_url = ''

            mes_images = []
            if not isinstance(item.data['images'], list):
                mes_images = [item.data['images']]
                marker_image = item.data['images'][0]
            else:
                mes_images = item.data['images']
                marker_image = item.data['images']

            clean_item = {
                "id": item._id,
                "latitude": latitude,
                "longitude": longitude,
                "location": item.data.get('location'),
                "title": item.data['title'],
                "marker_image": marker_image[0],
                "gallery": mes_images,
                "tags": item.data['tags'],
                "url": "detail.html",
                "description": item.data['body'],
                "author": u.id,
                "author_uuid": u.uuid,
                "author_name": u.full_name,
                "author_username": u.username,
                "time": item.time,
                "comments": clean_comments,
                "original_url": original_url
            }

            enriched_items.append(clean_item)

    return enriched_items


def ts_to_date(ts):
    """From timestamp to date format
    :param ts: timestamp to cast into date
    :return: date (%Y-%m-%d) in string format
    """
    if isinstance(ts, int):
        return datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    else:
        return None


def date_to_ts(date):
    """From date to timestamp format
    :param date: date (%Y-%m-%d) to cast into timestamp
    :return: timestamp in int format
    """
    # this is how the date should be 'YYYY-mm-dd' or 'YYYY-mm-dd HH:MM:SS'
    pattern = r'[\d]{4}[-/][\d]{2}[-/][\d]{2}'
    patternfull = r'[\d]{4}[-/][\d]{2}[-/][\d]{2} [\d]{2}\:[\d]{2}\:[\d]{2}'
    if date:
        if re.match(patternfull, date):
            dt = datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        elif re.match(pattern, date):
            dt = datetime.strptime(date, "%Y-%m-%d")
        return calendar.timegm(dt.timetuple())
    else:
        return None


def clean_insta_title(title=None):
    """Adapt long Instagram title to TnT title
    :param title: Instagram title
    :return: TnT title
    """
    result = ''

    if title is not None:
        tmp_title = remove_trailing_tags(title=title)

        if len(tmp_title) >= 70:
            result = tmp_title[:67]+'...'
        else:
            result = tmp_title

    return result


def clean_unsplash_tag(tag=None):
    """Clean unplash tag by removing space
    :param tag: tag to clean
    :return: clean tag
    """
    retour = None
    if tag is not None:
        retour = tag.replace(' ', '')

    return retour


def remove_trailing_tags(title=None):
    """Remove trailing tags from a string
    :param title: string to be cleaned
    :return: cleaned string
    """
    result = ''
    tag = r'#[^\s]*$'

    if title is not None:
        result = re.sub(tag, '', title, flags=re.M).strip()
        while re.search(tag, result, flags=re.M):
            result = re.sub(tag, '', result, flags=re.M).strip()

    return result


def get_geocode(place=None):
    """Get the geocode of a place, if possible
    :param place: string for the place
    :return: geocode (if found)
    """
    geocode_result = None

    if place is not None:
        gmaps = googlemaps.Client(key=app.config.get('GOOGLEMAPS_API_KEY'))
        geocode_result = gmaps.geocode(place)

    return geocode_result


def get_geoplace(lat=None, lon=None):
    """Get the geoplace of a geopoint, if possible
    :param lat: latitude of the point
    :param lon: longitude of the point
    :return: string for the place (if found)
    """
    retour = None

    if lat is not None and lon is not None:
        gmaps = googlemaps.Client(key=app.config.get('GOOGLEMAPS_API_KEY'))
        result = gmaps.reverse_geocode((lat, lon))

    if result is not None and len(result)>0:
        retour = result[0].get('formatted_address')

    return retour
