#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Item models definition"""

from flask import g
from elasticsearch import ElasticsearchException
from app import app


class TntItem(object):
    """Point in space and time"""
    def __init__(self, _id=None, location_d=None, time_t=None, data_d=None):
        """Define a TntItem object
        :param location_d: dict for geopoint {'lat': latitude, 'lon': longitude}
        :param time_t: tuple of (start_date, end_date)
        :param data_d: dict of data
        :return: the id of the item

        Data dictionnary
        {
            'title': 'string',
            'body': 'string',
            'author': 'integer',
            'instagram_id': 'string',
            'unsplash_id': 'string',
            'location': 'string',
            'tags': ['array of strings'],
            'images': ['array of strings'],
            'original_link': 'string'
        }
        """

        super(TntItem, self).__init__()
        if time_t is not None:
            (start_date, end_date) = time_t
        else:
            start_date = end_date = None

        self._id = _id
        if location_d is not None:
            self.location = location_d
        else:
            self.location = None
        self.time = {
            'start': start_date,
            'end': end_date,
        }
        self.data = data_d

    def save(self):
        """Save a TntItem in the ES"""
        doc = {
            'location': self.location,
            'time': self.time,
            'data': self.data,
        }

        if self._id:
            try:
                res = g.es.index(index='{}-{}'.format(app.config['ESINDEX'], 'item'), doc_type='doc', id=self._id, body=doc)
            except ElasticsearchException as e:
                app.logger.error('ES Error while saving %s: %s', (doc, e))
                self._id=None
        else:
            try:
                res = g.es.index(index='{}-{}'.format(app.config['ESINDEX'], 'item'), doc_type='doc', body=doc)
                self._id = res['_id']
            except ElasticsearchException as e:
                app.logger.error('ES Error while saving %s: %s', (doc, e))
                self._id=None

        return self._id

    def delete(self):
        """Delete a TntItem in the ES"""
        retour = False
        if self._id:
            try:
                res = g.es.delete(index='{}-{}'.format(app.config['ESINDEX'], 'item'), doc_type='doc', id=self._id)
                retour = res['found']
            except ElasticsearchException as e:
                app.logger.error('ES Error while deleting %s: %s' % (self._id, e))
                retour = False

        return retour


class TntItems(object):
    """Collection of TntItem"""
    def __init__(self, items=None):
        super(TntItems, self).__init__()
        self.items = []

        if items is not None:
            for item in items:
                self.items.append(item)

    def add(self, item=None):
        """Add a TntItem to the collection"""
        if item is not None:
            self.items.append(item)

    def search(self, text=None, tags=None, geobounds=None, start=None, end=None, author=None, sort=None, page=0):
        """Build a collection from a search to ES
        :param text: free text to search in title or body
        :param tags: field tags from ES (string)
        :param geobounds: geographical bounds for the search
        :param start: time.start boundary
        :param end: time.end boundary
        :param author: field author from ES (integer)
        :param sort: field to sort on (desc)
        :param page: pagination offset
        """
        query = {
            "query": {
                "bool": {
                    "must": [],
                    "should": [],
                    "filter": []
                }
            }
        }

        if text is not None and text != "":
            textconstraint = {
                "multi_match": {
                    "query": text,
                    "fields": ["data.title", "data.body"]
                }
            }
            query['query']['bool']['must'].append(textconstraint)

        if start is not None:
            if end is not None and end != start:
                timeconstraint = [{
                    "range": {"time.end": {"gte": start}}
                }, {
                    "range": {"time.start": {"lte": end}}
                }]
            else:
                timeconstraint = [{"range": {"time.start": {"gte": start}}}]
            query['query']['bool']['filter'].append(timeconstraint)

        if tags is not None and len(tags) > 0:
            tagsconstraint = []
            for tag in tags.split(','):
                tagsconstraint.append({"term": {"data.tags": tag}})
            query['query']['bool']['filter'].append(tagsconstraint)

        if author is not None and author != 'None':
            authorconstraint = {"term": {"data.author": author}}
            query['query']['bool']['must'].append(authorconstraint)

        if geobounds is not None and len(geobounds) > 0:
            if geobounds['northeast']['lat'] == geobounds['southwest']['lat']:
                geobounds['southwest']['lat'] += 1
            if geobounds['northeast']['lng'] == geobounds['southwest']['lng']:
                geobounds['southwest']['lng'] += 1
            geoconstraint = {
                'geo_bounding_box': {
                    'location': {
                        'top_right': {'lat': geobounds['northeast']['lat'], 'lon': geobounds['northeast']['lng']},
                        'bottom_left': {'lat': geobounds['southwest']['lat'], 'lon': geobounds['southwest']['lng']}
                    }
                }
            }
            query['query']['bool']['filter'].append(geoconstraint)

        app.logger.debug(query)

        # run the count query
        try:
            count = g.es.count(
                index='{}-{}'.format(app.config['ESINDEX'], 'item'),
                doc_type='doc',
                body=query)
        except ElasticsearchException as e:
            app.logger.error('ES Error while searching items (count) %s: %s', (query, e))
            count = {'count': 0}

        if sort is not None:
            query['sort'] = [{sort: "desc"}]

        query['size'] = app.config['ES_DEFAULT_SIZE']
        if int(page) > 0:
            query['from'] = int(page) * query['size']

        # app.logger.debug(query)

        try:
            cur = g.es.search(
                index='{}-{}'.format(app.config['ESINDEX'], 'item'),
                doc_type='doc',
                body=query)
        except ElasticsearchException as e:
            app.logger.error('ES Error while searching items %s: %s', (query, e))
            cur = None

        if cur is not None and cur['hits']['total'] > 0:
            for row in cur['hits']['hits']:
                self.add(TntItem(
                    _id=row['_id'],
                    time_t=(row['_source']['time']['start'], row['_source']['time']['end']),
                    location_d={'lat': row['_source']['location']['lat'], 'lon': row['_source']['location']['lon']},
                    data_d=row['_source']['data'],
                ))

        return count.get('count')


    def search_by_instagram(self, instagram_id=None):
        """Build a collection from a search to ES, restricting on instagram_id
        :param instagram_id: Instagram ID from object
        """
        query = {
            "query": {
                "bool": {
                    "filter": [{"match": {"data.instagram_id": instagram_id}}]
                }
            }
        }

        # app.logger.debug(query)

        # run the count query
        try:
            count = g.es.count(
                index='{}-{}'.format(app.config['ESINDEX'], 'item'),
                doc_type='doc',
                body=query)
        except ElasticsearchException as e:
            app.logger.error('ES Error while searching items by insta %s: %s', (query, e))
            count = {'count': 0}

        return count.get('count')


    def search_by_unsplash(self, unsplash_id=None):
        """Build a collection from a search to ES, restricting on unsplash_id
        :param unsplash_id: Unsplash ID from object
        """
        query = {
            "query": {
                "bool": {
                    "filter": [{"match": {"data.unsplash_id": unsplash_id}}]
                }
            }
        }

        # app.logger.debug(query)

        # run the count query
        try:
            count = g.es.count(
                index='{}-{}'.format(app.config['ESINDEX'], 'item'),
                doc_type='doc',
                body=query)
        except ElasticsearchException as e:
            app.logger.error('ES Error while searching items by unsplash %s: %s', (query, e))
            count = {'count': 0}

        return count.get('count')


    def get(self, ids=None):
        """Build a collection from a list of IDs
        :param id: list of ids
        """
        if ids is not None and len(ids) > 0:
            try:
                cur = g.es.mget(
                    index='{}-{}'.format(app.config['ESINDEX'], 'item'),
                    doc_type='doc',
                    body={'ids': ids})
            except ElasticsearchException as e:
                app.logger.error('ES Error while getting items %s: %s', (ids, e))
                cur = None

            if cur is not None and len(cur['docs']) > 0:
                for row in cur['docs']:
                    self.add(TntItem(
                        _id=row['_id'],
                        time_t=(row['_source']['time']['start'], row['_source']['time']['end']),
                        location_d={'lat': row['_source']['location']['lat'], 'lon': row['_source']['location']['lon']},
                        data_d=row['_source']['data'],
                    ))

            return True
        else:
            return False


    def delete(self):
        """Delete a collection from ES
        """
        for item in self.items:
            item.delete()

        del self.items[:]


class TntItemComment(object):
    """Comment associated to a TntItem"""
    def __init__(self, _id=None, item=None, text=None, source=None, author=None, date=None):
        """Define a TntItemComment object
        :param item: (string) referal item
        :param text: (string) message
        :param source: (string) source of the comment (could be 'instagram' or 'tnt')
        :param author: (string) author of the comment
        :param date: (timestamp) creation date
        :return: the id of the comment

        """
        super(TntItemComment, self).__init__()

        self._id = _id
        self.item = item
        self.text = text
        self.source = source
        self.author = author
        self.date = date


    def save(self):
        """Save a TntItem in the ES"""
        doc = {
            'item': self.item,
            'text': self.text,
            'source': self.source,
            'author': self.author,
            'date': self.date
        }

        if self._id:
            try:
                res = g.es.index(index='{}-{}'.format(app.config['ESINDEX'], 'comment'), doc_type='doc', id=self._id, body=doc)
            except ElasticsearchException as e:
                app.logger.error('ES Error while saving comment %s: %s', (doc, e))
                self._id = None
        else:
            try:
                res = g.es.index(index='{}-{}'.format(app.config['ESINDEX'], 'comment'), doc_type='doc', body=doc)
                self._id = res['_id']
            except ElasticsearchException as e:
                app.logger.error('ES Error while saving comment %s: %s', (doc, e))
                self._id = None

        return self._id


    def delete(self):
        """Delete a TntItemComment in the ES"""
        retour = False
        if self._id:
            try:
                res = g.es.delete(index='{}-{}'.format(app.config['ESINDEX'], 'comment'), doc_type='doc', id=self._id)
                retour = res['found']
            except ElasticsearchException as e:
                app.logger.error('ES Error while deleting %s: %s' % (self._id, e))
                retour = False

        return retour


class TntItemComments(object):
    """Collection of TntItemComment"""
    def __init__(self, items=None):
        super(TntItemComments, self).__init__()
        self.items = []

        if items is not None:
            for item in items:
                self.items.append(item)


    def add(self, item=None):
        """Add a TntItem to the collection"""
        if item is not None:
            self.items.append(item)


    def search(self, item=None, author=None, sort='date', page=0):
        """Build a collection from a search to ES
        :param item: (string) referal item
        :param author: (string) author of the comment
        :param sort: field to sort on (desc)
        :param page: pagination offset
        """
        query = {
            "query": {
                "bool": {
                    "must": [],
                    "filter": []
                }
            }
        }

        if item is not None and item != "":
            itemconstraint = {
                "match": {"refitem": item}
            }
            query['query']['bool']['filter'].append(itemconstraint)

        if author is not None:
            authorconstraint = {"term": {"author": author}}
            query['query']['bool']['must'].append(authorconstraint)

        # app.logger.debug(query)

        # run the count query
        try:
            count = g.es.count(
                index='{}-{}'.format(app.config['ESINDEX'], 'comment'),
                doc_type='doc',
                body=query)
        except ElasticsearchException as e:
            app.logger.error('ES Error while searching comment (count) %s: %s', (query, e))
            count = {'count': 0}

        if sort is not None:
            query['sort'] = [{sort: "desc"}]

        query['size'] = app.config['ES_DEFAULT_SIZE']
        if int(page) > 0:
            query['from'] = int(page) * query['size']

        try:
            cur = g.es.search(
                index='{}-{}'.format(app.config['ESINDEX'], 'comment'),
                doc_type='doc',
                body=query)
        except ElasticsearchException as e:
            app.logger.error('ES Error while searching comment %s: %s', (query, e))
            cur = None

        if cur is not None and cur['hits']['total'] > 0:
            for row in cur['hits']['hits']:
                self.add(TntItemComment(
                    _id=row['_id'],
                    item=row['_source']['item'],
                    text=row['_source']['text'],
                    source=row['_source']['source'],
                    author=row['_source']['author'],
                    date=row['_source']['date'],
                ))

        return count.get('count')


    def get(self, ids=None):
        """Build a collection from a list of IDs
        :param id: list of ids
        """
        if ids is not None and len(ids) > 0:
            try:
                cur = g.es.mget(
                    index='{}-{}'.format(app.config['ESINDEX'], 'comment'),
                    doc_type='doc',
                    body={'ids': ids})
            except ElasticsearchException as e:
                app.logger.error('ES Error while getting comments %s: %s', (ids, e))
                cur = None

            if cur is not None and len(cur['docs']) > 0:
                for row in cur['docs']:
                    self.add(TntItemComment(
                        _id=row['_id'],
                        item=row['_source']['item'],
                        text=row['_source']['text'],
                        source=row['_source']['source'],
                        author=row['_source']['author'],
                        date=row['_source']['date'],
                    ))

            return True
        else:
            return False


    def delete(self):
        """Delete a collection from ES
        """
        for item in self.items:
            item.delete()

        del self.items[:]
