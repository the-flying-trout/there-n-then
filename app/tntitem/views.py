#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This modules handles all the items actions
"""

# all the imports
import math
from flask import Blueprint, render_template, redirect, url_for, jsonify, flash, g, request, escape, session, make_response
from flask_login import login_required
from app.tntitem.models import TntItems, TntItem, TntItemComment
from app.tntitem.forms import SearchForm, DeleteForm, EditForm
from app.tntuser.models import User
from app.tntitem.utils import sync_insta_items, sync_unsplash_items, enrich_items, get_geocode
from app.resource import db
from app import app

# Define the blueprint: 'maps', set its url prefix: app.url
tntitem = Blueprint(
    'tntitem',
    __name__,
    url_prefix='/item',
    template_folder='templates',
    static_folder='../static')


@tntitem.route('/search', methods=['GET', 'POST'])
def search_items():
    """Function to search a list of items"""
    my_collection = TntItems()
    form = SearchForm(request.args, csrf_enabled=False)
    # this is the result of a real search (from right-pane)
    if form.validate():
        geobounds = {}
        if form.place.data is not None and form.place.data != "":
            geoplace = get_geocode(place=form.place.data)
            if geoplace is not None and len(geoplace) > 0:
                geobounds = geoplace[0]['geometry']['bounds']
        _ = my_collection.search(
            text=form.keyword.data,
            tags=form.tags.data,
            geobounds=geobounds,
            start=form.event_start.data,
            end=form.event_end.data,
            sort='time.start')
    # this is a search from the map display
    else:
        if request.args.get('author') != 'None':
            myAuthor = User.get_by_username(request.args.get('author'))
        else:
            myAuthor = None
        if myAuthor is not None:
            myAuthorId = myAuthor.id
        else:
            myAuthorId = None
        if ('north_east_lat' in request.args and
                'north_east_lng' in request.args and
                'south_west_lat' in request.args and
                'south_west_lng' in request.args):
            geobounds = {
                'northeast':{
                    'lat': float(request.args.get('north_east_lat')),
                    'lng': float(request.args.get('north_east_lng'))
                },
                'southwest': {
                    'lat': float(request.args.get('south_west_lat')),
                    'lng': float(request.args.get('south_west_lng'))
                }
            }
            _ = my_collection.search(
                author=myAuthorId,
                geobounds=geobounds,
                sort='time.start')
        else:
            _ = my_collection.search(
                author=myAuthorId,
                sort='time.start')

    items = enrich_items(items=my_collection.items)
    return jsonify(items)


@tntitem.route('/data_sync')
@login_required
def data_sync():
    """Function to sync data from social accounts with local data"""
    u = User.get_by_id(g.user.id)
    if not u.is_instagram and not u.is_unsplash:
        flash('You need to connect your social account (Instagram/Unsplash)', 'danger')
        return redirect(url_for('tntuser.profile'))
    else:
        response = make_response(redirect(url_for('tntitem.show_items')))
        if u.is_instagram:
            if u.insta_last_id is None:
                response.set_cookie('tour', value="three", path=url_for('tntitem.show_items'))
            task = sync_insta_items.delay(u.id)
            session['instagram_sync_task_id'] = task.id
            flash('Instagram sync started', 'success')
        if u.is_unsplash:
            if u.unsplash_last_id is None:
                response.set_cookie('tour', value="three", path=url_for('tntitem.show_items'))
            task = sync_unsplash_items.delay(u.id)
            session['unsplash_sync_task_id'] = task.id
            flash('Unsplash sync started', 'success')

        return response


@tntitem.route('/instagram_sync') 
def instagram_sync(): 
    """Function to sync instagram account with local data""" 
    u = User.get_by_id(g.user.id) 
    if not u.is_instagram: 
        flash('You need to connect your Instagram account', 'danger') 
        return redirect(url_for('tntuser.profile')) 
    else: 
        response = make_response(redirect(url_for('tntitem.show_items'))) 
        if u.insta_last_id is None: 
            response.set_cookie('tour', value="three", path=url_for('tntitem.show_items')) 
        task = sync_insta_items.delay(u.id) 
        session['instagram_sync_task_id'] = task.id 
        flash('Instagram crawl started', 'success') 
        return response 

@tntitem.route('/unsplash_sync') 
@login_required 
def unsplash_sync(): 
    """Function to sync Unsplash account with local data""" 
    u = User.get_by_id(g.user.id) 
    if not u.is_unsplash: 
        flash('You need to connect your Unsplash account', 'danger') 
        return redirect(url_for('tntuser.profile')) 
    else: 
        response = make_response(redirect(url_for('tntitem.show_items'))) 
        task = sync_unsplash_items.delay(u.id) 
        session['unsplash_sync_task_id'] = task.id 
        flash('Unsplash crawl started', 'success') 
        return response 


@tntitem.route('/instasync_status/<task_id>')
@login_required
def instagram_sync_status(task_id):
    task = sync_insta_items.AsyncResult(task_id)
    if task.state == 'SUCCESS':
        session.pop('instagram_sync_task_id', None)

    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'total': 0,
            'status': 'Pending'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'total': task.info.get('total', 0),
            'status': task.info.get('status', '')
        }
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'total': 0,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


@tntitem.route('/unsplasync_status/<task_id>')
@login_required
def unsplash_sync_status(task_id):
    task = sync_unsplash_items.AsyncResult(task_id)
    if task.state == 'SUCCESS':
        session.pop('unsplash_sync_task_id', None)

    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'total': 0,
            'status': 'Pending'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'total': task.info.get('total', 0),
            'status': task.info.get('status', '')
        }
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'total': task.info.get('total', 0),
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


@tntitem.route('/sidebar_results')
def show_sidebar_items():
    """Function to display the items in the sidebar"""
    markers = request.args.getlist('markers[]')

    my_collection = TntItems()
    my_collection.get(ids=markers)
    items = enrich_items(items=my_collection.items)
    return render_template(
        'sidebar_results.html',
        items=items)


@tntitem.route('/my', defaults={'page': 1})
@tntitem.route('/my/<page>')
@login_required
def show_items(page):
    """Function to display a list of items"""
    my_collection = TntItems()
    total = my_collection.search(author=g.user.id, sort='time.start', page=int(page)-1)
    for i, _ in enumerate(my_collection.items):
        if (isinstance(my_collection.items[i].data['images'], list) and
                len(my_collection.items[i].data['images'])):
            my_collection.items[i].data['images'] = my_collection.items[i].data['images'][0]

    return render_template(
        'item_list.html',
        items=my_collection.items,
        current_page=page,
        max_page=math.ceil(total/app.config['ES_DEFAULT_SIZE']))


@tntitem.route('/edit/<iditem>', methods=['GET', 'POST'])
@login_required
def edit_item(iditem):
    """Function to display the item edit page"""
    items = TntItems()
    items.get(ids=[iditem])
    if len(items.items) > 0:
        item = items.items[0]

    if request.method == 'POST':
        time_start = int(request.form['event_start'])
        time_end = int(request.form['event_end'])
        loc_latitude = float(request.form['latitude'])
        loc_longitude = float(request.form['longitude'])
        mes_images = []
        for mon_image in request.form.getlist('images-item[]'):
            if mon_image != "":
                mes_images.append(mon_image)
        mes_tags = request.form['tags'].split(',')
        for i, tag in enumerate(mes_tags):
            mes_tags[i] = tag.strip()
        # remove empty tags
        mes_tags = list(filter(None, mes_tags))
        app.logger.debug('Tags: %s' % mes_tags)

        form = EditForm(
            request.form,
            images=mes_images,
            time_t=(time_start, time_end),
            location_d={'lat': loc_latitude, 'lon': loc_longitude},
            csrf_enabled=True)

        if form.validate_on_submit():
            try:
                item = TntItem(
                    _id=iditem,
                    location_d={'lat': loc_latitude, 'lon': loc_longitude},
                    time_t=(time_start, time_end),
                    data_d={
                        'title': form.title.data,
                        'body': form.body.data,
                        'author': g.user.id,
                        'tags': mes_tags,
                        'images': mes_images,
                        'location': form.location.data,
                        'instagram_id': form.instagram_id.data,
                        'unsplash_id': form.unsplash_id.data,
                        'original_url': form.original_url.data,
                    })
            except ValueError:
                flash('Unable to edit TnT', 'danger')

            try:
                item.save()
            except:
                flash('Unable to save TnT', 'danger')
            return redirect(url_for('tntitem.show_items'))
        else:
            flash('Error while editing', 'danger')

    else:
        if not isinstance(item.data['images'], list):
            mes_images = [item.data['images']]
        else:
            mes_images = item.data['images']
        form = EditForm(
            iditem=item._id,
            title=item.data['title'],
            body=item.data['body'],
            instagram_id=item.data.get('instagram_id'),
            unsplash_id=item.data.get('unsplash_id'),
            original_url=item.data.get('original_url'),
            location=item.data['location'],
            tags=item.data['tags'],
            images=mes_images,
            event_start=item.time['start'],
            event_end=item.time['end'],
            latitude=item.location['lat'],
            longitude=item.location['lon'],
            csrf_enabled=True)

    return render_template(
        'modal_edit.html',
        iditem=item._id,
        item=item,
        form=form,
        error=form.errors)


@tntitem.route('/show_delete/<iditem>', methods=['GET'])
@login_required
def delete_modal(iditem):
    """Function to display the item delete modal"""
    items = TntItems()
    items.get(ids=[iditem])
    if len(items.items) > 0:
        item = items.items[0]

    form = DeleteForm(iditem=iditem, csrf_enabled=True)
    return render_template('modal_delete.html', form=form, title=item.data['title'])


@tntitem.route('/delete', methods=['POST'])
@login_required
def delete_item():
    """Function to delete an item"""
    form = DeleteForm(request.form, csrf_enabled=True)
    if form.validate_on_submit():
        try:
            my_tnt = TntItem(_id=form.iditem.data)
        except ValueError:
            flash('Unable to find item '+form.iditem.data, 'danger')
        user_tnts = TntItems()
        user_tnts.get(ids=[my_tnt._id])
        if len(user_tnts.items)>0:
            user_id = user_tnts.items[0].data.get('author')
            title = user_tnts.items[0].data.get('title')
            u = User.get_by_id(user_id)
            u.increment_items_count(-1)
            db.session.add(u)
            db.session.commit()

        if not my_tnt.delete():
            flash('Unable to delete item '+title, 'danger')
        else:
            flash(title+' has bee permantely deleted', 'success')
    return redirect(url_for('tntitem.show_items'))


@tntitem.route('/item')
def show_modal_item():
    """Function to display the modal view of an item"""
    id_item = request.args.get('id')

    my_collection = TntItems()
    my_collection.get(ids=[id_item])
    items = enrich_items(items=my_collection.items)
    return render_template(
        'modal_item.html',
        items=items)


@tntitem.route('/add_comment', methods=['GET'])
@login_required
def add_comment():
    """Function to add a comment on an item"""
    my_comment = TntItemComment(
        item=request.args.get('item'),
        text=escape(request.args.get('text')),
        source='tnt',
        author=g.user.id,
        date=request.args.get('curdate'))
    my_comment.save()

    return jsonify({'comment': str(my_comment)})
