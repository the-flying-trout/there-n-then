#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""External ressources definition and initialization"""

import os
from elasticsearch import Elasticsearch
from instagram.client import InstagramAPI
from unsplash_python.unsplash import Unsplash
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask import Flask
from celery import Celery
from app import app

CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", default="redis://127.0.0.1:6379/0")
CELERY_BROKER_BACKEND = os.environ.get("CELERY_BROKER_BACKEND", default="redis://127.0.0.1:6379/0")

app.config['CELERY_BROKER_URL'] = CELERY_BROKER_URL
app.config['CELERY_RESULT_BACKEND'] = CELERY_BROKER_BACKEND

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])

bcrypt = Bcrypt()

login_manager = LoginManager()

db = SQLAlchemy()

def connect_es():
    """Create a connection to ElasticSearch, and return it on success"""
    es = Elasticsearch([app.config['ESHOST']])
    if not es.ping():
        raise ValueError("ES Connection failed")
    else:
        return es

def init_mail():
    """Create a Flask-Mail instance"""
    return Mail(app)

def get_instagram_api(key, secret, url):
    """Return an instance of the Instagram API"""
    return InstagramAPI(
        client_id=key,
        client_secret=secret,
        redirect_uri=url
    )

def get_unsplash_api(key, secret, url):
    """Return an instance of the Unsplash API"""
    return Unsplash({
        'application_id': key,
        'secret': secret,
        'callback_url': url
    })
