#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This modules handles all the user actions
"""

# all the imports
from flask import Blueprint, render_template, redirect, url_for, g, request, flash, send_from_directory
from app.tntuser.forms import RegisterForm
from app.tntpage.forms import ContactForm
from app.tntuser.utils import send_email
from app import app


# Define the blueprint: 'maps', set its url prefix: app.url
tntpage = Blueprint('tntpage', __name__, template_folder='templates', static_folder='../static')


@tntpage.route('/')
def show_home():
    """Function to display the homepage if user not logged in"""
    if request.args.get('ref') == "producthunt":
        return redirect(url_for('tntmap.show_map', ref='producthunt'))
    else:
        return redirect(url_for('tntmap.show_map'))


@tntpage.route('/terms')
def show_terms():
    """Function to display the static terms and conditions page"""
    return render_template('terms.html')


@tntpage.route('/privacy')
def show_privacy():
    """Function to display the static privacy policy page"""
    return render_template('privacy.html')


@tntpage.route('/credits')
def show_credits():
    """Function to display the static credits page"""
    return render_template('credits.html')


@tntpage.route('/contact', methods=['GET', 'POST'])
def show_contact():
    """Function to display the static contact page"""
    if request.method == 'POST':
        form = ContactForm(request.form, csrf_enabled=True)
        if form.validate():
            body = render_template(
                'contact_mail.txt',
                content=form.message.data,
                user=form.name.data,
                email=form.email.data)
            send_email(
                app.config['MAIL_DEFAULT_SENDER'],
                app.config['MAIL_DEFAULT_CONTACT'],
                form.subject.data, body)
            flash('Your message has been sent, thanks!', 'success')
            return redirect(url_for('tntpage.show_contact'))

    else:
        form = ContactForm(csrf_enabled=True)

    return render_template(
        'contact.html',
        form=form,
        error=form.errors)



@tntpage.route('/robots.txt')
def show_robotstxt():
    """Function to return robots.txt file
    """
    return send_from_directory(app.static_folder, 'robots.txt')