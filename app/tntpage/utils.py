#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utility functions for static pages"""
from app import app

def send_mail(user=None, email=None, subject=None, message=None):
    """Sends a mail to TNT (for contact)
    :param user: Name of the sender
    :param email: Email of the sender
    :param subject: Subject of the mail
    :param message: Body of the mail
    :return: boolean on operation status
    """
    app.logger.debug('Mail to be sent from {} <{}> about [{}]:\n{}'.format(
        user, email, subject, message))

    return True
