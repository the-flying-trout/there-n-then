#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Forms for the static pages."""

from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField
from wtforms.validators import DataRequired, Optional, Email
from app import app

class ContactForm(FlaskForm):
    """Contact form."""

    name = TextField(
        'Name',
        validators=[Optional()]
    )
    email = TextField(
        'Email',
        validators=[DataRequired(), Email()]
    )
    subject = TextField(
        'Subject',
        validators=[DataRequired()]
    )
    message = TextAreaField(
        'Message',
        validators=[DataRequired()]
    )

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(ContactForm, self).__init__(*args, **kwargs)

    def validate(self):
        """Validate the form."""
        initial_validation = super(ContactForm, self).validate()

        if not initial_validation:
            app.logger.debug(self.errors)
            return False

        return True
