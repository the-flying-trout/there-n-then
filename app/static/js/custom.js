////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// jQuery
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var resizeId;
var lastModal;
var defaultColor;
var originalNavigationCode;
var navigationIsTouchingBrand;
var responsiveNavigationTriggered = false;

$(document).ready(function($) {
    "use strict";

    originalNavigationCode = $(".primary-nav").html();

    if( $("body").hasClass("navigation-fixed") ){
        fixedNavigation(true);
    }
    else {
        fixedNavigation(false);
    }

    if( $(".tse-scrollable").length ){
        $(".tse-scrollable").TrackpadScrollEmulator();
    }

    heroSectionHeight();

// Render hero search form ---------------------------------------------------------------------------------------------

    $("select").on("rendered.bs.select", function () {
        $('head').append( $('<link rel="stylesheet" type="text/css">').attr('href', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css') );
        if( !viewport.is('xs') ){
            $(".search-form.vertical").css( "top", ($(".hero-section").height()/2) - ($(".search-form .wrapper").height()/2) );
        }
        trackpadScroll("initialize");
    });

    if( !viewport.is('xs') ){
        $(".search-form.vertical").css( "top", ($(".hero-section").height()/2) - ($(".search-form .wrapper").height()/2) );
        trackpadScroll("initialize");
    }

//  Social Share -------------------------------------------------------------------------------------------------------

    if( $(".social-share").length ){
        socialShare(myShareURL);
    }

//  iCheck -------------------------------------------------------------------------------------------------------------

    if ($("input[type=checkbox]").length > 0) {
        $("input").iCheck();
    }

    if ($("input[type=radio]").length > 0) {
        $("input").iCheck();
    }

//  Smooth Scroll ------------------------------------------------------------------------------------------------------

    $('.main-nav a[href^="#"], a[href^="#"].scroll').on('click',function (e) {
        e.preventDefault();
        if( $(this).hasClass("to-top") ){
            $('html, body').stop().animate({
                'scrollTop': 0
            }, 2000, 'swing');
        }
        else {
            var target = this.hash,
                $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 2000, 'swing', function () {
                window.location.hash = target;
            });
        }
    });

//  Modal after click --------------------------------------------------------------------------------------------------

    $("[data-modal-external-file], .quick-detail").live("click", function(e){
        e.preventDefault();
        var modalTarget, modalFile;
        if( $(this).closest(".item").attr("data-id") ){
            modalTarget = $(this).closest(".item").attr("data-id");
            modalFile = "/item";
        }
        else {
            modalTarget = $(this).attr("data-target");
            modalFile = $(this).attr("data-modal-external-file");
        }
        if( $(this).attr("data-close-modal") == "true" ){
            lastModal.modal("hide");
            setTimeout(function() {
                openModal(modalTarget, modalFile);
            }, 400);
        }
        else {
            openModal(modalTarget, modalFile);
        }
    });

//  Multiple modal hack ------------------------------------------------------------------------------------------------

    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

//  Map in Row listing -------------------------------------------------------------------------------------------------

    $(".item.item-row").each(function() {
        var element = "map"+$(this).attr("data-id");
        var place;
        $(this).find(".map").attr("id", element );
        var _latitude = $(this).attr("data-latitude");
        var _longitude = $(this).attr("data-longitude");
        if( $(this).attr("data-address") ){
            place = $(this).attr("data-address");
        }
        else {
            place = false;
        }
        simpleMap(_latitude,_longitude, element, false, place);
    });

//  Close "More" menu on click anywhere on page ------------------------------------------------------------------------

    $(document).on("click", function(e){
        if( e.target.className == "controls-more" ){
            $(".controls-more.show").removeClass("show");
            $(e.target).addClass("show");

        }
        else {
            $(".controls-more.show").each(function() {
                $(this).removeClass("show");
            });
        }
    });

// Mobile navigation button --------------------------------------------------------------------------------------------

    $(".nav-btn").on("click", function(){
        $(this).toggleClass("active");
        $(".primary-nav").toggleClass("show");
    });

//  Duplicate desired element ------------------------------------------------------------------------------------------

    $(".duplicate").live("click", function(e){
        e.preventDefault();
        var duplicateElement = $(this).attr("href");
        var parentElement = $(duplicateElement)[0].parentElement;
        $(parentElement).append( $(duplicateElement)[0].outerHTML );
    });

//  Enable image previews in multi file input --------------------------------------------------------------------------

    if( $("input[type=file].with-preview").length ){
        $("input.file-upload-input").MultiFile({
            list: ".file-upload-previews"
        });
    }

//  DateRange picker ---------------------------------------------------------------------------------------------------

    if( $('input[name="daterangepicker"]').length > 0 ){
        var start_input = document.getElementById('event_start');
        var end_input = document.getElementById('event_end');

        if( start_input.value == null || start_input.value == '' ){
            var default_start = moment().startOf('year');
        } else {
            var default_start = moment(start_input.value);
        }
        if( end_input.value == null || end_input.value == '' ){
            var default_end = moment();
        } else {
            var default_end = moment(end_input.value);
        }

        $('input[name="daterangepicker"]').daterangepicker({
            locale: {
              format: 'YYYY-MM-DD'
            },
            ranges: {
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
               'This Year': [moment().startOf('year'), moment().endOf('year')],
               'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
            showCustomRangeLabel: false,
            alwaysShowCalendars: true,
            startDate: default_start,
            endDate: default_end,
            opens: "left"
        }, function(start, end, label) {
                start_input.value = start.format('YYYY-MM-DD');
                end_input.value = end.format('YYYY-MM-DD');
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });
    };

//  Tags ---------------------------------------------------------------------------------------------------------------
    if( $('#input-tags').length > 0 ){
        $('#input-tags').selectize({
            plugins: ['remove_button'],
            persist: false,
            createOnBlur: false,
            valueField: 'tag',
            labelField: 'tag',
            searchField: ['tag'],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' + escape(item.tag) + '</div>';
                }
            },
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: "/tag/search",
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        tag: query,
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);
                    }
                });
            },
        });
    };

    equalHeight(".container");
    bgTransfer();
    responsiveNavigation();

//  Tour ---------------------------------------------------------------------------------------------------------------
    var tourTempalte = `
        <div class='popover tour'>
          <div class='arrow'></div>
          <h3 class='popover-title'></h3>
          <div class='popover-content'></div>
          <div class='popover-navigation'>
            <button class='btn btn-default' data-role='end'>End tour</button>
          </div>
        </div>`;
    var tourLink = document.getElementById("restartTour");
    if ($('#restartTour').length>0){
        tourLink.onclick = function () {
            $.cookie('tour', 'one', { path: location.pathname });
            $.removeCookie('tourState');
        };
    }
    if ($.cookie("tour") == "one") {
        // Instance the tour
        var tour = new Tour({
            steps: [
            {
                element: "#username",
                title: "Say your name",
                content: "Hey, you should add your name, so we know how to call you."
            }],
            onEnd: function(tour){
                $.removeCookie('tour', { path: location.pathname });
            },
            storage: false,
            template: tourTempalte,
        });
    } else if ($.cookie("tour") == "oneplus") {
        // Instance the tour
        var tour = new Tour({
            steps: [
            {
                element: "#instagram_connect",
                title: "Connect you Instagram account",
                content: "In order to have your Instagram data, you need to connect your account."
            }],
            onEnd: function(tour){
                $.removeCookie('tour', { path: location.pathname });
            },
            storage: false,
            template: tourTempalte,
        });
    } else if ($.cookie("tour") == "two") {
        // Instance the tour
        var tour = new Tour({
            steps: [
            {
                element: "#data_refresh",
                title: "Sync your data",
                content: "You need to sync your Instagram/Unsplash data so it appears on the map.",
                placement: "left",
            }],
            onEnd: function(tour){
                $.removeCookie('tour', { path: location.pathname });
            },
            storage: false,
            template: tourTempalte,
        });
    } else if ($.cookie("tour") == "three") {
        // Instance the tour
        var tour = new Tour({
            steps: [
            {
                element: "#mymap",
                title: "View your data",
                content: "Now your data is being synced with your Instagram, you can visualise the map of your posts."
            }],
            onEnd: function(tour){
                $.removeCookie('tour', { path: location.pathname });
                $.cookie('tourState', 'end')
            },
            storage: false,
            template: tourTempalte,
        });
    };
    if (typeof tour !== 'undefined') {
        // Initialize the tour
        tour.init();

        // Start the tour
        tour.start(true) || tour.restart();
    };

//  Username dupecheck -------------------------------------------------------------------------------------------------
    if( $('#username').length > 0 ){
        var tooltips = $( "#username" ).tooltip({
          position: {
            my: "left top",
            at: "right+5 top-5",
            collision: "none"
          }
        });
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 1 second for example
        var $input = $('#username');
        var initialValue = document.getElementById("username").value;

        $('#username').keyup(function(){
            clearTimeout(typingTimer);
            if ($('#username').val()) {
                typingTimer = setTimeout(function() {doneTyping(initialValue)}, doneTypingInterval);
            }
        });

        //user is "finished typing," checks uniqueness
        function doneTyping (initialValue) {
            var currentValue = document.getElementById("username").value;
            if (currentValue != initialValue && currentValue != "") {
                var validationurl = "/user/check_username/" + currentValue;
                $.ajax({
                    url: validationurl,
                    type: "GET",
                    error: function () {},
                    dataType: 'json',
                    success : function (response) {
                        if (response.unique) {
                            document.getElementById("username").style.background = "transparent";
                            $('#username').tooltip( "destroy" );
                        } else {
                            if (! document.getElementById("username").classList.contains('form-control-error')) {
                                document.getElementById("username").style.background = "rgba(255, 0, 0, 0.3)";
                                document.getElementById("username").setAttribute('title', 'There could be only one '+currentValue+' and we already have one :(') ;
                                $('#username').tooltip("show");
                            }
                        };
                    }
                });
            } else if (currentValue == initialValue) {
                document.getElementById("username").style.background = "transparent";
                $('#username').tooltip( "destroy" );
            }
        }
    }

//  Form validators ----------------------------------------------------------------------------------------------------
    if( $('#reset_password').length > 0 ){
        $('#reset_password').validate({
          rules: {
            current_password: "required",
            new_password: "required",
            confirm_new_password: {
              equalTo: "#new_password"
            }
          }
        });
    };
    if( $('#registration_form').length > 0 ){
        $('#registration_form').validate({
          rules: {
            email: {
                required: true,
                email: true,
            },
            password: "required",
            confirm_password: {
                equalTo: "#password"
            }
          }
        });
    };

});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// On Load
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(window).load(function(){
    initializeOwl();
});

$(window).resize(function(){
    clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 250);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function clickAndDisable(link) {
    // change look and feel
    link.className = "btn btn-primary btn-small btn-framed btn-rounded btn-light-frame icon shadow add-listing";
    // disable subsequent clicks
    link.onclick = function(event) {
        event.preventDefault();
    }
}   


function updateLastSync(status_url, status_div) {
    // send GET request to status URL
    $.getJSON(status_url, function(data) {
        // update UI
        nbitem = parseInt(data['total']);
        if (data['state'] == 'PENDING') {
            $(status_div).text('Sync will start soon..');
            // rerun in 30 seconds
            setTimeout(function() {
                location.reload();
            }, 30000);
        }
        else if (data['state'] == 'PROGRESS') {
            $(status_div).text('already '+ data['total'] + ' items crawled, and more to go');
            // rerun in 30 seconds
            setTimeout(function() {
                location.reload();
            }, 30000);
        }
        else if (data['state'] == 'SUCCESS') {
            $(status_div).text('Items crawled: ' + data['total']);
        }
        else {
            $(status_div).text('Items crawled: ' + data['total'] + ' | status: ' + data['status']);
        }
    });
}


function heroSectionHeight(){

    if( $(".hero-section").length > 0 ){
        if( viewport.is('xs') ){
            $(".map-wrapper").height( $(window).height() - 25 );
            $(".hero-section").height( $(".hero-section .map-wrapper").height() +  $(".hero-section .search-form").height() + $(".hero-section .results").height() + 40 );
            $(".has-background").css( "min-height", $(window).height() - $("#page-header").height() + "px" );
        }
        else {
            if( $("body").hasClass("navigation-fixed") ){
                $(".hero-section.full-screen").height( $(window).height() - $("#page-header nav").height() );
                $(".hero-section .map-wrapper").css( "height", "100%" );
            }
            else {
                $(".hero-section.full-screen").height( $(window).height() - $("#page-header").height() );
                $(".hero-section .map-wrapper").css( "height", "100%" );
                if( $(".map-wrapper").length > 0 ){
                    reloadMap();
                }
            }
        }
        if( !viewport.is('xs') ){
            $(".search-form.vertical").css( "top", ($(".hero-section").height()/2) - ($(".search-form .wrapper").height()/2) );
        }
    }

}

function openModal(target, modalPath){

    $("body").append('<div class="modal modal-external fade" id="'+ target +'" tabindex="-1" role="dialog" aria-labelledby="'+ target +'"><i class="loading-icon fa fa-circle-o-notch fa-spin"></i></div>');

    $("#" + target + ".modal").on("show.bs.modal", function () {
        var _this = $(this);
        lastModal = _this;
        $.ajax({
            // url: "static/external/" + modalPath,
            url: modalPath,
            method: "GET",
            //dataType: "html",
            data: { id: target },
            success: function(results){
                _this.append(results);
                $('head').append( $('<link rel="stylesheet" type="text/css">').attr('href', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css') );
                $(".selectpicker").selectpicker();
                _this.find(".gallery").addClass("owl-carousel");
                var img = _this.find(".gallery img:first")[0];
                if( img ){
                    $(img).load(function() {
                        timeOutActions(_this);
                    });
                }
                else {
                    timeOutActions(_this);
                }
                socialShare(myShareURL);
                _this.on("hidden.bs.modal", function () {
                    $(lastClickedMarker).removeClass("active");
                    $(".pac-container").remove();
                    _this.remove();
                });
            },
            error : function (e) {
                console.log(e);
            }
        });

    });

    $("#" + target + ".modal").modal("show");

    function timeOutActions(_this){
        setTimeout(function(){
            if( _this.find(".map").length ){
                if( _this.find(".modal-dialog").attr("data-latitude") && _this.find(".modal-dialog").attr("data-longitude") ){
                    simpleMap( _this.find(".modal-dialog").attr("data-latitude"), _this.find(".modal-dialog").attr("data-longitude"), "map-modal", _this.find(".modal-dialog").attr("data-marker-drag") );
                }
                else {
                    simpleMap( 0, 0, "map-modal", _this.find(".modal-dialog").attr("data-marker-drag"), _this.find(".modal-dialog").attr("data-address") );
                }
            }
            initializeOwl();
            initializeReadMore();
            _this.addClass("show");
        }, 200);

    }

}

//  Transfer "img" into CSS background-image

function bgTransfer(){
    //disable-on-mobile
    if( viewport.is('xs') ){

    }
    $(".bg-transfer").each(function() {
        $(this).css("background-image", "url("+ $(this).find("img").attr("src") +")" );
    });
}

function socialShare(myShareURL){
    var socialButtonsEnabled = 1;
    if ( socialButtonsEnabled == 1 ){
        $('head').append( $('<link rel="stylesheet" type="text/css">').attr('href', '//cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css') );
        $.getScript( "//cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js", function( data, textStatus, jqxhr ) {
            $(".social-share").jsSocials({
                showLabel: false,
                showCount: false,
                url: myShareURL,
                text: "Checkout my map on There 'n then!",
                shares: ["twitter", "facebook", "whatsapp"]
            });
        });
    }
}

function initializeOwl(){
    if( $(".owl-carousel").length ){
        $(".owl-carousel").each(function() {

            var items = parseInt( $(this).attr("data-owl-items"), 10);
            if( !items ) items = 1;

            var nav = parseInt( $(this).attr("data-owl-nav"), 2);
            if( !nav ) nav = 0;

            var dots = parseInt( $(this).attr("data-owl-dots"), 2);
            if( !dots ) dots = 0;

            var center = parseInt( $(this).attr("data-owl-center"), 2);
            if( !center ) center = 0;

            var loop = parseInt( $(this).attr("data-owl-loop"), 2);
            if( !loop ) loop = 0;

            var margin = parseInt( $(this).attr("data-owl-margin"), 2);
            if( !margin ) margin = 0;

            var autoWidth = parseInt( $(this).attr("data-owl-auto-width"), 2);
            if( !autoWidth ) autoWidth = 0;

            var navContainer = $(this).attr("data-owl-nav-container");
            if( !navContainer ) navContainer = 0;

            var autoplay = $(this).attr("data-owl-autoplay");
            if( !autoplay ) autoplay = 0;

            var fadeOut = $(this).attr("data-owl-fadeout");
            if( !fadeOut ) fadeOut = 0;
            else fadeOut = "fadeOut";

            if( $("body").hasClass("rtl") ) var rtl = true;
            else rtl = false;

            $(this).owlCarousel({
                navContainer: navContainer,
                animateOut: fadeOut,
                autoplaySpeed: 2000,
                autoplay: autoplay,
                autoheight: 1,
                center: center,
                loop: loop,
                margin: margin,
                autoWidth: autoWidth,
                items: items,
                nav: nav,
                dots: dots,
                autoHeight: true,
                rtl: rtl,
                navText: []
            });

            if( $(this).find(".owl-item").length == 1 ){
                $(this).find(".owl-nav").css( { "opacity": 0,"pointer-events": "none"} );
            }

        });
    }
}

function trackpadScroll(method){
    if( method == "initialize" ){
        if( $(".results-wrapper").find("form").length ) {
            if( !viewport.is('xs') ){
                $(".results-wrapper .results").height( $(".results-wrapper").height() - $(".results-wrapper .form")[0].clientHeight );
            }
        }
    }
    else if ( method == "recalculate" ){
        setTimeout(function(){
            if( $(".tse-scrollable").length ){
                $(".tse-scrollable").TrackpadScrollEmulator("recalculate");
            }
        }, 1000);
    }
}

// Do after resize

function doneResizing(){
    var $equalHeight = $('.container');
    for( var i=0; i<$equalHeight.length; i++ ){
        equalHeight( $equalHeight );
    }
    responsiveNavigation();
    heroSectionHeight();
}

// Responsive Navigation

function responsiveNavigation(){

    if( viewport.is('xs') ){

        $("body").addClass("nav-btn-only");

        if( $("body").hasClass("nav-btn-only") && responsiveNavigationTriggered == false ){
            responsiveNavigationTriggered = true;
            $(".primary-nav .has-child").children("a").attr("data-toggle", "collapse");
            $(".primary-nav .has-child").find(".nav-wrapper").addClass("collapse");
            $(".mega-menu .heading").each(function(e) {
                $(this).wrap("<a href='" + "#mega-menu-collapse-" + e + "'></a>");
                $(this).parent().attr("data-toggle", "collapse");
                $(this).parent().addClass("has-child");
                $(this).parent().attr("aria-controls", "mega-menu-collapse-"+e);
            });
            $(".mega-menu ul").each(function(e) {
                $(this).attr("id", "mega-menu-collapse-"+e);
                $(this).addClass("collapse");
            });
        }
    }
    else {
        navigationIsTouchingBrand = false;
        responsiveNavigationTriggered = false;
        $("body").removeClass("nav-btn-only");
        $(".primary-nav").html("");
        $(".primary-nav").html(originalNavigationCode);
    }
}

function equalHeight(container){
    if( !viewport.is('xs') ){
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = new Array(),
            $el,
            topPosition = 0;

        $(container).find('.equal-height').each(function() {
            $el = $(this);
            //var marginBottom = $el.css("margin-bottom").replace("px", "");
            //console.log( $el.css("margin-bottom").replace("px", "") );
            $($el).height('auto');
            topPostion = $el.position().top;
            if (currentRowStart != topPostion) {
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }
}

// Viewport ------------------------------------------------------------------------------------------------------------

var viewport = (function() {
    var viewPorts = ['xs', 'sm', 'md', 'lg'];

    var viewPortSize = function() {
        return window.getComputedStyle(document.body, ':before').content.replace(/"/g, '');
    };

    var is = function(size) {
        if ( viewPorts.indexOf(size) == -1 ) throw "no valid viewport name given";
        return viewPortSize() == size;
    };

    var isEqualOrGreaterThan = function(size) {
        if ( viewPorts.indexOf(size) == -1 ) throw "no valid viewport name given";
        return viewPorts.indexOf(viewPortSize()) >= viewPorts.indexOf(size);
    };

    // Public API
    return {
        is: is,
        isEqualOrGreaterThan: isEqualOrGreaterThan
    }

})();

// Read more -----------------------------------------------------------------------------------------------------------

function initializeReadMore(){

    $.ajax({
        type: "GET",
        url: "https://cdnjs.cloudflare.com/ajax/libs/Readmore.js/2.2.0/readmore.min.js",
        success: readMoreCallBack,
        dataType: "script",
        cache: true
    });

    function readMoreCallBack(){
        var collapseHeight;
        var $readMore = $(".read-more");
        if( $readMore.attr("data-collapse-height") ){
            collapseHeight =  parseInt( $readMore.attr("data-collapse-height"), 10 );
        }else {
            collapseHeight = 55;
        }
        $readMore.readmore({
            speed: 500,
            collapsedHeight: collapseHeight,
            blockCSS: 'display: inline-block; width: auto; min-width: 120px;',
            moreLink: '<a href="#" class="btn btn-primary btn-xs btn-light-frame btn-framed btn-rounded">More<i class="icon_plus"></i></a>',
            lessLink: '<a href="#" class="btn btn-primary btn-xs btn-light-frame btn-framed btn-rounded">Less<i class="icon_minus-06"></i></a>'
        });
    }
}

function fixedNavigation(state){
    if( state == true ){
        $("body").addClass("navigation-fixed");
        var headerHeight = $("#page-header").height();
        $("#page-header").css("position", "fixed");
        $("#page-content").css({
            "-webkit-transform" : "translateY(" + headerHeight + "px)",
            "-moz-transform"    : "translateY(" + headerHeight + "px)",
            "-ms-transform"     : "translateY(" + headerHeight + "px)",
            "-o-transform"      : "translateY(" + headerHeight + "px)",
            "transform"         : "translateY(" + headerHeight + "px)"
        });
    }
    else if( state == false ) {
        $("body").removeClass("navigation-fixed");
        $("#page-header").css("position", "relative");
        $("#page-content").css({
            "-webkit-transform" : "translateY(0px)",
            "-moz-transform"    : "translateY(0px)",
            "-ms-transform"     : "translateY(0px)",
            "-o-transform"      : "translateY(0px)",
            "transform"         : "translateY(0px)"
        });
    }
}

//  Show element after desired time ------------------------------------------------------------------------------------

if( !viewport.is('xs') ){
    var messagesArray = [];
    $("[data-toggle=popover]").popover({
        template: '<div class="popover" role="tooltip"><div class="close"><i class="fa fa-close"></i></div><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });
    $(".popover .close").live('click',function () {
        $(this).closest(".popover").popover("hide");
    });
    $("[data-show-after-time]").each(function() {
        var _this = $(this);
        setTimeout(function(){
            if( _this.attr("data-toggle") == "popover" ){
                _this.popover("show");
            }
            else {
                for( var i=0; i < messagesArray.length; i++ ){
                    $(messagesArray[i]).css("bottom", parseInt( $(messagesArray[i]).css("bottom") ) + _this.context.clientHeight + 10 );
                }
                messagesArray.push(_this);
                _this.addClass("show");
                if( _this.attr("data-close-after-time") ){
                    setTimeout(function(){
                        closeThis();
                    }, _this.attr("data-close-after-time") );
                }
            }
        }, _this.attr("data-show-after-time") );
        $(this).find(".close").on("click",function () {
            closeThis();
        });
        function closeThis(){
            _this.removeClass("show");
            setTimeout(function(){
                _this.remove();
            }, 400 );
        }
    });

}

//  Show element when scrolled desired amount of pixels ----------------------------------------------------------------

$("[data-show-after-scroll]").each(function() {
    var _this = $(this);
    var scroll = _this.attr("data-show-after-scroll");
    var offsetTop = $(this).offset().top;
    $(window).scroll(function() {
        var currentScroll = $(window).scrollTop();
        if (currentScroll >= scroll) {
            _this.addClass("show");
        }
        else {
            _this.removeClass("show");
        }
    });
});