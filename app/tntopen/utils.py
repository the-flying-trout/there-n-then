#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utility functions for static pages"""
from app.resource import db
from sqlalchemy.sql import func
from app.tntuser.models import User
from app import app

def count_items():
    """Count the number of tntitems in the ES
    :return: items count (int)
    """
    items_count = db.session.query(func.sum(User.items_count)).scalar()
    return items_count or 0

def max_items():
    """Get the max count of tntitems for a user in the ES
    :return: items count (int)
    """
    items_count = db.session.query(func.max(User.items_count)).scalar()
    return items_count or 0

def count_users():
    """Count the number of tntusers in the DB
    :return: users count (int)
    """
    users_count = db.session.query(func.count(User.uuid)).scalar()
    return users_count or 0
