#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This modules handles all the user actions
"""

# all the imports
from flask import Blueprint, render_template, g, request, flash
from app.tntopen.utils import count_items, max_items, count_users
from app import app


# Define the blueprint: 'open', set its url prefix: app.url
tntopen = Blueprint(
    'tntopen',
    __name__,
    url_prefix='/open',
    template_folder='templates',
    static_folder='../static')


@tntopen.route('/')
def show_open():
    """Function to display the open"""
    items_count = count_items()
    items_max = max_items()
    users_count = count_users()
    return render_template(
        'open.html',
        items_count=items_count,
        items_max=items_max,
        users_count=users_count,
        )
