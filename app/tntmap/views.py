#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This modules handles all the maps actions
"""

# all the imports
from flask import Blueprint, render_template, request, redirect, url_for, g, flash
from app.tntitem.forms import SearchForm


# Define the blueprint: 'maps', set its url prefix: app.url
tntmap = Blueprint(
    'tntmap',
    __name__,
    url_prefix='/map',
    template_folder='templates',
    static_folder='../static')

@tntmap.route('/', defaults={'username': None})
@tntmap.route('/@<username>')
def show_map(username):
    """Function to display a map (with content)"""
    if not g.user.is_authenticated:
        if request.args.get('ref') == "producthunt":
            people = 'Hi Producthunters!'
        else:
            people = 'Hi, travelers, digital nomads, photographers, hospitality professionals!'
        flash('{} We all like to keep track of our travel history. There \'n Then displays your pictures in time and space directly on a map. So, create your personal map to share it with your community by registering!'.format(people), 'success')

    return render_template(
        'map.html',
        searchform=SearchForm(request.form, csrf_enabled=False),
        constraintAuthor=username)
