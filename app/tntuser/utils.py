#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utility functions for User management"""

import string
import random
import time
from itsdangerous import URLSafeTimedSerializer
from flask import g, url_for, render_template
from flask_mail import Message
from app.tntuser.models import User
from app.tntitem.models import TntItem, TntItems, TntItemComment, TntItemComments
from app.resource import celery, connect_es, init_mail, db
from app import app

def generate_confirmation_token(email, key, salt):
    """Generate a confirmation token from the user' email"""
    serializer = URLSafeTimedSerializer(key)
    return serializer.dumps(email, salt=salt)


def confirm_token(token, key, salt, expiration=3600):
    """Check the confirmation tocken"""
    serializer = URLSafeTimedSerializer(key)
    try:
        email = serializer.loads(token, salt=salt, max_age=expiration)
    except:
        return False
    return email


def send_email(sender, to, subject, template):
    """Send a mail"""
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=sender
    )
    g.mail.send(msg)


def generate_password(size=8, char=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    """Function to generate random passwords"""
    return ''.join(random.choice(char) for _ in range(size))


def send_validation_email(strategy, backend, code):
    """Sends a validation mail to a newly created user
    see http://psa.matiasaguirre.net/docs/pipeline.html#email-validation

    :param strategy: The current strategy instance
    :param backend: The current backend instance
    :param code: code is a model instance used to validate the email address,
        it contains three fields (code, email, verified)
    :return: the mail sent with Flask_Mail
    """
    app.logger.debug('getting into validation email with %s and %s' % (code.code, code.email))

    url = url_for(
        'social.complete',
        backend=strategy.backend_name,
        _external=True) + '?verification_code=' + code.code

    msg = Message('TNT - validate your account',
                  recipients=[code.email])
    msg.body = 'Hello\n\nplease validate your mail here %s' % (url)

    g.mail.send(msg)
    return True


def is_username_unique(username):
    """Test if a username is unique in the DB
    :param username: username to be tested (string)
    :return: boolean
    """
    retour = True

    if username is not None:
        compte = User.get_username_count(username)
        if compte > 0:
            retour=False

    return retour


def delete_items(user):
    """Function to delete all items for a user
    :param user: a User object
    :return: count of items and comments deleted (tuple)
    """
    count_items = 0
    count_comments = 0

    if user:
        user_items = TntItems()
        user_items_count = user_items.search(author=user.id)
        while user_items_count>0:
            app.logger.debug('found %s items to be removed' % user_items_count)
            count_items += len(user_items.items)
            # Delete item comments first
            for item in user_items.items:
                user_items_comments = TntItemComments()
                user_items_comments_count = user_items_comments.search(item=item._id)
                while user_items_comments_count>0:
                    count_comments += len(user_items_comments.items)
                    user_items_comments.delete()
                    user_items_comments = TntItemComments()
                    user_items_comments_count = user_items_comments.search(item=item._id)
            # Then delete the item itself
            user_items.delete()
            app.logger.debug('%s left in collection' % len(user_items.items))
            user_items = TntItems()
            user_items_count = user_items.search(author=user.id)

    return (count_items, count_comments)


def delete_comments(user):
    """Function to delete all comments from a user
    :param user: a User object
    :return: count of comments deleted
    """
    count_comments = 0

    if user:
        user_items_comments = TntItemComments()
        user_items_comments_count = user_items_comments.search(author=user.id)
        while user_items_comments_count>0:
            count_comments += len(user_items_comments.items)
            user_items_comments.delete()
            user_items_comments = TntItemComments()
            user_items_comments_count = user_items_comments.search(author=user.id)

    return count_comments


def delete_user(user):
    """Function to delete a user
    :param user: a User object
    :return: boolean
    """
    if user:
        db.session.delete(user)
        db.session.commit()

    return True


@celery.task(bind=True)
def process_delete_user(self, user_id):
    """Function to delete a user account
    after cleaning all content linked (comments, items)
    :param user_id: a User id
    :return: boolean
    """
    retour = False
    count_items = 0
    count_items_comments = 0
    count_user_comments = 0

    with app.app_context():
        g.es = connect_es()
        g.mail = init_mail()

        user = User.get_by_id(user_id)

        (count_items, count_comments) = delete_items(user)
        count_user_comments = delete_comments(user)

        send_email(
            sender=app.config.get('MAIL_SUPPORT_CONTACT'),
            to=user.email,
            subject='There n Then - Account deleted',
            template=render_template(
                'delete_email.html',
                full_name=user.full_name,
                count_items=count_items,
                count_items_comments=count_items_comments,
                count_user_comments=count_user_comments),
        )

        delete_user(user)

        return {
            'count_items': count_items,
            'count_items_comments': count_items_comments,
            'count_user_comments': count_user_comments,
            'status': 'done'
        }
