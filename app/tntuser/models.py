#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""User and Role models definition"""

import datetime as dt
from flask_login import UserMixin
from sqlalchemy.orm import relationship
from app.resource import db, bcrypt
from app import app

class Role(db.Model):
    """User roles."""

    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    user_id = db.Column(db.ForeignKey('users.id'), nullable=True)
    user = relationship('User', backref='roles')

    def __init__(self, name, **kwargs):
        """Init the role."""
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        """repr."""
        return '<Role({name})>'.format(name=self.name)


class User(UserMixin, db.Model):
    """The user model."""

    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(40), unique=True)
    username = db.Column(db.String(80), unique=True, nullable=True)
    email = db.Column(db.String(254), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=True)  # the hashed password
    items_count = db.Column(db.Integer, default=0)  # the count of TnTItems
    insta_id = db.Column(db.String(128), nullable=True)  # the instagram id
    insta_user = db.Column(db.String(128), nullable=True)  # the instagram username
    insta_token = db.Column(db.String(128), nullable=True)  # the instagram token
    insta_last_id = db.Column(db.String(50), nullable=True)  # instagram id of the last synced item
    insta_current_sync = db.Column(db.String(50), nullable=True)  # celery job ID for the current instagram sync process
    unsplash_id = db.Column(db.String(128), nullable=True)  # the unsplash id
    unsplash_user = db.Column(db.String(128), nullable=True)  # the unsplash username
    unsplash_token = db.Column(db.String(128), nullable=True)  # the unsplash token
    unsplash_last_id = db.Column(db.String(50), nullable=True)  # unsplash id of the last synced item
    unsplash_current_sync = db.Column(db.String(50), nullable=True)  # celery job ID for the current unsplash sync process
    created_at = db.Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    active = db.Column(db.Boolean(), default=False)

    def __init__(self, email, password=None, **kwargs):
        """Init the user name."""
        db.Model.__init__(self, email=email, **kwargs)
        if password:
            self.set_password(password)
        else:
            self.password = None

        self.items_count = 0

    @classmethod
    def get_by_id(cls, _id):
        """Get the user by ``_id``."""
        can_to_int = any(
            (isinstance(_id, str) and _id.isdigit(),
             isinstance(_id, (int, float))),
        )
        if can_to_int:
            return cls.query.get(int(_id))
        return None

    @classmethod
    def get_by_uuid(cls, uuid):
        """Get the user by ``uuid``."""
        return cls.query.filter_by(uuid=uuid).first()

    @classmethod
    def get_by_username(cls, username):
        """Get the user by ``username``."""
        return cls.query.filter_by(username=username).first()

    @classmethod
    def get_username_count(cls, username):
        """Check if a ``username`` exists."""
        return cls.query.filter_by(username=username).count()

    @property
    def is_active(self):
        """This property should return True if this is an active user"""
        return self.active

    @property
    def is_authenticated(self):
        """This property should return True if the user is authenticated"""
        if self.is_active:
            return True
        else:
            return False

    def set_password(self, password):
        """Set the password."""
        self.password = bcrypt.generate_password_hash(password)

    def increment_items_count(self, count=1):
        """Add the count to items_count."""
        if self.items_count and isinstance(self.items_count, int):
            self.items_count += count
        else:
            self.items_count = count

    def check_password(self, value):
        """Validate the password."""
        try:
            retour = bcrypt.check_password_hash(self.password, value)
        except ValueError as e:
            app.logger.error('Unable to check password: {}'.format(e))
            retour = False
        return retour

    @property
    def is_social(self):
        """Check if a user is instagram or unsplash connected."""
        return self.is_instagram or self.is_unsplash

    @property
    def is_instagram(self):
        """Check if a user is instagram connected."""
        return self.insta_id is not None and self.insta_token is not None

    @property
    def is_unsplash(self):
        """Check if a user is unsplash connected."""
        return self.unsplash_id is not None and self.unsplash_token is not None

    @property
    def full_name(self):
        """Display username."""
        if self.username is not None and self.username != "":
            return "{0}".format(self.username)
        else:
            if self.is_instagram:
                return "{0}".format(self.insta_user)
            else:
                return "Anonymous"

    def __repr__(self):
        """repr."""
        return '<User({email!r})>'.format(email=self.email)
