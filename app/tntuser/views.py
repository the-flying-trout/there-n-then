#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This modules handles all the user actions"""

# all the imports
import json
import requests
import uuid
from flask import Blueprint, render_template, redirect, url_for, request, flash, g, session, make_response, jsonify
from flask_login import login_required, login_user, logout_user
from app.resource import login_manager, db, get_instagram_api, get_unsplash_api
from app.tntuser.forms import RegisterForm, LoginForm, ProfileForm, \
    ChangePasswordForm, ResetPasswordForm, DeleteAccountForm
from app.tntuser.models import User
from app.tntuser.utils import generate_confirmation_token, confirm_token, send_email, \
    generate_password, is_username_unique, process_delete_user
from app import app


# Define the blueprint: 'tntuser', set its url prefix: app.url
tntuser = Blueprint(
    'tntuser',
    __name__,
    url_prefix='/user',
    template_folder='templates',
    static_folder='../static')

@tntuser.route('/register', methods=['GET', 'POST'])
def register():
    """Function to create a user from a registration form"""
    form = RegisterForm(request.form, csrf_enabled=True)
    if request.method == 'POST':
        if form.validate_on_submit():
            u = User(
                uuid=str(uuid.uuid4()),
                email=form.email.data,
                password=form.password.data,
                active=False
            )
            db.session.add(u)
            db.session.commit()

            token = generate_confirmation_token(
                u.email, app.config['SECRET_KEY'], app.config['SECURITY_PASSWORD_SALT'])
            confirm_url = app.config['ROOT_URL'] + url_for('tntuser.confirm_email', token=token)
            html = render_template('activate_email.html', confirm_url=confirm_url)
            subject = "TnT - Please confirm your email"
            send_email(app.config['MAIL_DEFAULT_SENDER'], u.email, subject, html)

            flash('A confirmation email has been sent via email.', 'success')
            return redirect(url_for('tntmap.show_map'))
        else:
            return render_template('register.html', form=form, error=form.errors)
    else:
        return render_template('register.html', form=form, error=form.errors)


@tntuser.route('/confirm/<token>', methods=['GET'])
def confirm_email(token):
    """Function to validate token and activate user account"""
    response = make_response(redirect(url_for('tntuser.profile')))

    try:
        email = confirm_token(
            token, app.config['SECRET_KEY'], app.config['SECURITY_PASSWORD_SALT'])
    except:
        flash('The confirmation link is invalid or has expired.', 'danger')
    else:
        user = User.query.filter_by(email=email).first_or_404()
        if user.active:
            flash('Account already confirmed. Please login.', 'success')
        else:
            user.active = True
            response.set_cookie('tour', value="one", path=url_for('tntuser.profile'))
            db.session.add(user)
            db.session.commit()
            flash('You have confirmed your account. Thanks!', 'success')
            login_user(user)

    return response


@tntuser.route('/signin', methods=['GET', 'POST'])
def login():
    """Function to display the modal sign in form"""
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            login_user(form.user)
            redirect_url = request.args.get('next') or url_for('tntmap.show_map')
            return redirect(redirect_url)
    if form.errors:
        return render_template('signin.html', form=form, error=form.errors)
    return render_template('modal_signin.html', form=form, error=form.errors)


@tntuser.route('/reset_pass', methods=['GET', 'POST'])
def reset_password():
    """Function to reset the password"""
    if request.method == 'POST':
        form = ResetPasswordForm(request.form, csrf_enabled=True)
        if form.validate_on_submit():
            new_password = generate_password()
            u = User.query.filter_by(email=form.email.data).first()
            u.set_password(new_password)
            db.session.commit()

            html = render_template('new_password_email.html', password=new_password)
            subject = "Please confirm your email"
            send_email(app.config['MAIL_DEFAULT_SENDER'], u.email, subject, html)

            flash('Your password has been updated', 'success')

        return redirect(url_for('tntmap.show_map'))

    else:
        form = ResetPasswordForm(csrf_enabled=True)

    return render_template('modal_reset_password.html', form=form, error=form.errors)


@tntuser.route('/change_pass', methods=['POST'])
@login_required
def change_password():
    """Function to change the password"""
    form_reset = ChangePasswordForm(request.form, csrf_enabled=True)
    if form_reset.validate_on_submit():
        u = User.get_by_id(g.user.id)
        u.set_password(form_reset.new_password.data)
        db.session.commit()
        flash('Your password has been updated', 'success')
    else:
        flash('Unable to change your password', 'danger')

    return redirect(url_for('tntuser.profile'))


@tntuser.route('/check_username/<username>')
@login_required
def check_username(username):
    return jsonify({'unique': is_username_unique(username)})


@tntuser.route('/me', methods=['GET', 'POST'])
@login_required
def profile():
    """Function to display the profile form"""
    form_reset = ChangePasswordForm(csrf_enabled=True)

    if request.method == 'POST':
        form = ProfileForm(request.form, csrf_enabled=True)
        if form.validate_on_submit():
            u = User.get_by_id(g.user.id)
            u.username = form.username.data
            db.session.commit()
    else:
        form = ProfileForm(username=g.user.username, email=g.user.email, csrf_enabled=True)

    response = make_response(render_template(
        'profile.html',
        form=form, error=form.errors,
        form_reset=form_reset, error_reset=form_reset.errors))

    if request.cookies.get('tourState') != 'end':
        u = User.get_by_id(g.user.id)
        if u.username:
            if u.is_instagram:
                if u.items_count>0:
                    response.set_cookie('tour', value="three", path=url_for('tntitem.show_items'))
                else:
                    response.set_cookie('tour', value="two", path=url_for('tntuser.profile'))
            else:
                response.set_cookie('tour', value="oneplus", path=url_for('tntuser.profile'))
        else:
            response.set_cookie('tour', value="one", path=url_for('tntuser.profile'))

    return response


@tntuser.route('/instagram_connect')
@login_required
def instagram_connect():
    """Connect to Instagram"""
    instapi = get_instagram_api(
        app.config['INSTAGRAM_KEY'],
        app.config['INSTAGRAM_SECRET'],
        app.config['ROOT_URL'] + url_for('tntuser.instagram_callback'))
    url = instapi.get_authorize_url(scope=app.config['INSTAGRAM_SCOPE'])
    return redirect(url)


@tntuser.route('/instagram_callback')
@login_required
def instagram_callback():
    """Instagram will redirect users back to this route after successfully logging in"""
    code = request.args.get('code')
    response = make_response(redirect(url_for('tntuser.profile')))

    if code:
        try:
            r = requests.post("https://api.instagram.com/oauth/access_token", data={
                'client_id': app.config['INSTAGRAM_KEY'],
                'client_secret': app.config['INSTAGRAM_SECRET'],
                'grant_type': 'authorization_code',
                'redirect_uri': app.config['ROOT_URL'] + url_for('tntuser.instagram_callback'),
                'code': code})
        except requests.exceptions.ConnectionError as e:
            app.logger.error('unable to connect to Instagram - %s', e)
        except requests.exceptions.RequestException as e:
            app.logger.error('unable to connect to Instagram - %s', e)

        if r.status_code != 200:
            app.logger.debug('Could not get access token: %s', r.reason)
            flash('Unable to connect to Instagram', 'danger')

        else:
            retour = json.loads(r.text)
            access_token = retour.get('access_token')
            user = retour.get('user')

            if not access_token:
                flash('Unable to get an Instagram access token', 'danger')

            # Sessions are used to keep this data
            session['instagram_access_token'] = access_token
            session['instagram_user'] = user

            # Store the data in database too
            g.user.insta_id = user['id']
            g.user.insta_user = user['username']
            g.user.insta_token = access_token
            db.session.commit()

            response.set_cookie('tour', value="two", path=url_for('tntuser.profile'))

            flash('Instagram succesfully connected', 'success')
    else:
        flash('Unable to get a valid Instagram code', 'danger')

    return response


@tntuser.route('/instagram_disconnect')
@login_required
def instagram_disconnect():
    """Disconnect from Instagram"""
    g.user.insta_id = None
    g.user.insta_user = None
    g.user.insta_token = None
    db.session.commit()
    session['instagram_access_token'] = None
    session['instagram_user'] = None

    return redirect(url_for('tntuser.profile'))


@tntuser.route('/unsplash_connect')
@login_required
def unsplash_connect():
    """Connect to Unsplash"""
    unsplapi = get_unsplash_api(
        app.config['UNSPLASH_KEY'],
        app.config['UNSPLASH_SECRET'],
        app.config['ROOT_URL'] + url_for('tntuser.unsplash_callback'))
    url = unsplapi.auth().get_authentication_url(app.config['UNSPLASH_SCOPE'])
    return redirect(url)


@tntuser.route('/unsplash_callback')
@login_required
def unsplash_callback():
    """Unsplash will redirect users back to this route after successfully logging in"""
    code = request.args.get('code')
    response = make_response(redirect(url_for('tntuser.profile')))

    if code:
        try:
            r = requests.post("https://unsplash.com/oauth/token", data={
                'client_id': app.config['UNSPLASH_KEY'],
                'client_secret': app.config['UNSPLASH_SECRET'],
                'grant_type': 'authorization_code',
                'redirect_uri': app.config['ROOT_URL'] + url_for('tntuser.unsplash_callback'),
                'code': code})
        except requests.exceptions.ConnectionError as e:
            app.logger.error('unable to connect to Unsplash - %s', e)
        except requests.exceptions.RequestException as e:
            app.logger.error('unable to connect to Unsplash - %s', e)

        if r.status_code != 200:
            app.logger.debug('Could not get access token: %s', r.reason)
            flash('Unable to connect to Unsplash', 'danger')

        else:
            retour = json.loads(r.text)
            access_token = retour.get('access_token')

            if not access_token:
                flash('Unable to get an Instagram access token', 'danger')
            else:
                headers = {"Authorization":"Bearer {}".format(access_token)}

                try:
                    r = requests.get("https://api.unsplash.com/me", headers=headers)
                except requests.exceptions.ConnectionError as e:
                    app.logger.error('unable to connect to Unsplash - %s', e)
                except requests.exceptions.RequestException as e:
                    app.logger.error('unable to connect to Unsplash - %s', e)

                if r.status_code != 200:
                    app.logger.debug('Could not get user profile: %s', r.reason)
                    flash('Unable to get your Unsplash profile', 'danger')
                else:
                    user = json.loads(r.text)
                    session['unsplash_user'] = user.get('username')
                    g.user.unsplash_id = user.get('id')
                    g.user.unsplash_user = user.get('username')

                # Sessions are used to keep this data
                session['unsplash_access_token'] = access_token

                # Store the data in database too
                g.user.unsplash_token = access_token
                db.session.commit()

            flash('Unsplash succesfully connected', 'success')
    else:
        flash('Unable to get a valid Unsplash code', 'danger')

    return response


@tntuser.route('/unsplash_disconnect')
@login_required
def unsplash_disconnect():
    """Disconnect from Unsplash"""
    g.user.unsplash_id = None
    g.user.unsplash_user = None
    g.user.unsplash_token = None
    db.session.commit()
    session['unsplash_access_token'] = None

    return redirect(url_for('tntuser.profile'))


@login_manager.user_loader
def load_user(_id):
    """Function to load the user from DB"""
    return User.get_by_id(int(_id))


@tntuser.route('/missing_username', methods=['GET'])
@login_required
def show_missing_username():
    """Function to display the alert modal when username is missing"""
    return render_template('modal_missing_username.html')


@tntuser.route('/logout/')
@login_required
def logout():
    """Function to sign-out the user"""
    logout_user()
    return redirect(url_for('tntmap.show_map'))


@tntuser.route('/show_delete', methods=['GET'])
@login_required
def delete_modal():
    """Function to display the user delete modal"""
    return render_template('modal_delete_user.html', form=DeleteAccountForm(uuid=g.user.uuid, csrf_enabled=True))


@tntuser.route('/delete', methods=['POST'])
@login_required
def delete_user():
    """Function to delete a user"""
    form=DeleteAccountForm(request.form, csrf_enabled=True)
    if form.validate_on_submit():
        try:
            u = User.get_by_uuid(form.uuid.data)
        except ValueError as e:
            flash('Unable to find user '+form.uuid.data, 'danger')
        else:
            task = process_delete_user.delay(u.id)
            if task:
                flash('Your account is being removed. The whole process takes time, so we do it asynchronously, but we\'ll send you a last mail to confirm when it\'s done', 'success')
            else:
                send_email(
                    sender=app.config.get('MAIL_SUPPORT_CONTACT'),
                    to=app.config.get('MAIL_SUPPORT_CONTACT'),
                    subject='Error while dropping account - {}'.format(form.uuid.data),
                    template='{} asked for its account to be deleted, but I couldn\'t.'.format(u.email))
                flash('There was an issue while cleaning your account (support has been notified, and will get back to you shortly).', 'danger')
    else:
        app.logger.debug(form.errors)

    return redirect(url_for('tntuser.logout'))
