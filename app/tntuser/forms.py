#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Forms for the user blueprint."""

from flask import g
from flask_wtf import FlaskForm
from wtforms import PasswordField, TextField, HiddenField
from wtforms.validators import DataRequired, Email, Length
from app.tntuser.models import User
from app.tntuser.utils import is_username_unique

class RegisterForm(FlaskForm):
    """Registration form."""

    email = TextField(
        'Email',
        validators=[DataRequired(), Email(), Length(min=6, max=254)]
    )
    password = PasswordField(
        'Password',
        validators=[DataRequired(), Length(min=6, max=255)]
    )
    confirm_password = PasswordField(
        'Confirm Password',
        validators=[DataRequired(), Length(min=6, max=255)]
    )

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(RegisterForm, self).validate()

        if not initial_validation:
            return False
        email = User.query.filter_by(email=self.email.data).first()

        if email:
            self.email.errors.append('Email already registered')
            return False

        if self.password.data != self.confirm_password.data:
            self.confirm_password.errors.append('Passwords do not match')
            return False

        return True

class LoginForm(FlaskForm):
    """Login form."""

    email = TextField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(LoginForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.query.filter_by(email=self.email.data).first()
        if not self.user:
            self.email.errors.append('Unknown email address')
            return False

        if not self.user.check_password(self.password.data):
            self.password.errors.append('Invalid password')
            return False

        if not self.user.active:
            self.email.errors.append('User not activated')
            return False
        return True

class ProfileForm(FlaskForm):
    """Profile form."""

    username = TextField('Username', id='username', validators=[])
    email = TextField('Email', validators=[Email()])

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(ProfileForm, self).validate()
        if not initial_validation:
            return False

        if not is_username_unique(self.username.data):
            self.username.errors.append('Username should be unique')
            return False

        self.user = User.query.filter_by(email=self.email.data).first()
        if not self.user:
            self.email.errors.append('Unknown email address')
            return False

        if not self.user.active:
            self.email.errors.append('User not activated')
            return False
        return True

class ChangePasswordForm(FlaskForm):
    """Reset password form."""

    current_password = PasswordField('Current Password', validators=[DataRequired()])
    new_password = PasswordField(
        'New Password', validators=[DataRequired(), Length(min=6, max=255)])
    confirm_new_password = PasswordField(
        'Confirm New Password', validators=[DataRequired(), Length(min=6, max=255)])

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(ChangePasswordForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.get_by_id(g.user.id)
        if not self.user:
            self.errors.append('Unknown user')
            return False

        if not self.user.check_password(self.current_password.data):
            self.current_password.errors.append('Invalid password')
            return False

        if self.new_password.data != self.confirm_new_password.data:
            self.confirm_new_password.errors.append('Passwords do not match')
            return False

        return True

class ResetPasswordForm(FlaskForm):
    """Reset password form."""

    email = TextField(
        'Email',
        validators=[DataRequired(), Email(), Length(min=6, max=254)]
    )

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(ResetPasswordForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(ResetPasswordForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.query.filter_by(email=self.email.data).first()
        if not self.user:
            return False

        return True

class DeleteAccountForm(FlaskForm):
    """Delete account form."""

    uuid = HiddenField(
        'uuid',
        validators=[DataRequired()]
    )

    def __init__(self, *args, **kwargs):
        """Init the form."""
        super(DeleteAccountForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(DeleteAccountForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.get_by_uuid(uuid=self.uuid.data)
        if not self.user:
            return False

        return True
