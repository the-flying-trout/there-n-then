#!/bin/bash
#
# celery script

readonly PROGNAME=$(basename "$0")
readonly PROGDIR=$(dirname "$0")
readonly LOGFILE="$PROGDIR/log/celery.log"
readonly VENVBIN="$PROGDIR/venv/bin/activate"
readonly APP_CONFIG_FILE_DOCKER="$PROGDIR/config/docker.py"
readonly APP_CONFIG_FILE_PRODUCTION="$PROGDIR/config/production.py"
readonly APP_CONFIG_FILE_PREPRODUCTION="$PROGDIR/config/preproduction.py"
readonly LOGLEVEL_PRODUCTION="info"
readonly LOGLEVEL_PREPRODUCTION="debug"
readonly LOGLEVEL_DOCKER="debug"

if [[ "$#" -gt "0" ]]; then
    if [[ "$1" == "production" ]]; then
        readonly LOGLEVEL="$LOGLEVEL_PRODUCTION"
        export APP_CONFIG_FILE="$APP_CONFIG_FILE_PRODUCTION"
        export CELERY_BROKER_URL="redis://127.0.0.1:6380/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6380/0"
    elif [[ "$1" == "preproduction" ]]; then
        readonly LOGLEVEL="$LOGLEVEL_PREPRODUCTION"
        export APP_CONFIG_FILE="$APP_CONFIG_FILE_PREPRODUCTION"
        export CELERY_BROKER_URL="redis://127.0.0.1:6379/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6379/0"
    elif [[ "$1" == "docker" ]]; then
        readonly LOGLEVEL="$LOGLEVEL_DOCKER"
        export APP_CONFIG_FILE="$APP_CONFIG_FILE_DOCKER"
        export CELERY_BROKER_URL="redis://127.0.0.1:6379/0"
        export CELERY_BROKER_BACKEND="redis://127.0.0.1:6379/0"
    fi
fi

# shellcheck source=/dev/null
source "$VENVBIN"

exec celery -A app.resource:celery worker \
  -l $LOGLEVEL >> "$LOGFILE" 2>&1
